var path = require ('path');
var fs = require ('fs');
const webpack = require('webpack');

const {
    addDecoratorsLegacy,
    babelInclude,
    disableEsLint,
} = require("customize-cra");

const ignoreWarnings = value => config => {
    config.ignoreWarnings = value;
    return config;
};

module.exports = function override(config) {
    const fallback = config.resolve.fallback || {};
    Object.assign(fallback, {
        "crypto": require.resolve("crypto-browserify"),
        "stream": require.resolve("stream-browserify"),
        "assert": require.resolve("assert"),
        "http": require.resolve("stream-http"),
        "https": require.resolve("https-browserify"),
        "os": require.resolve("os-browserify"),
        "url": require.resolve("url")
    })
    config.resolve.fallback = fallback;
    config.plugins = (config.plugins || []).concat([
        new webpack.ProvidePlugin({
            process: 'process/browser',
            Buffer: ['buffer', 'Buffer']
        })
    ])
    disableEsLint(),
    addDecoratorsLegacy(),
    /*Make sure Babel compiles the stuff in the common folder*/
    babelInclude([
        path.resolve('src'), // don't forget this
        fs.realpathSync('node_modules/envelop-client-core/src')
    ]),
    ignoreWarnings([/Failed to parse source map/])
    return config;
}