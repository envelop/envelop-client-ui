import {
	BigNumber,
	ChainType,
	ERC20Type
} from "envelop-client-core";

export type StateType = {
	_advancedLoading : undefined | AdvancedLoaderType,
	_info            : undefined | InfoModalProps,
	account: {
		userAddress: string,
		balanceNative: BigNumber,
	} | undefined,
	availableChains: Array<ChainType>,
	currentChain?: ChainType,
	erc20List: Array<ERC20Type>,
	appConfig?: AppConfigType,
};

export type InfoMessageType = {
	text: string,
	link_text: string,
	link_url: string,
	isClosable: boolean,
	closed?: boolean,
}
export type AppConfigType = {
	INFO_MESSAGES: Array<InfoMessageType>,
	SWARM_BASE_URLS: Array<{ prefix: string, baseUrl: string }>,
	CHAIN_SPECIFIC_DATA: ChainConfigType,
}

export type ChainConfigType = {
	chain                         : ChainType,
	wrapperContract               : string,
	checkerContract?              : string,
	WNFTStorageContracts          : Array<{ address: string, standart: string }>,
	supportedERC20Tokens          : Array<string>,
	marketplaceUrl?               : string;
	minterContract?               : string;
	crossing?                     : {
		keeper      : string,
		spawner     : string,
		targetChains: Array<{ targetChainId: number, address: string, icon?: string, name?: string }>,
	};
	nftMinterContract721?         : string;
	nftMinterContract1155?        : string;
	batchWorker?                  : string;
	batchWrapSupportedERC20Tokens?: Array<string>;
	INFO_MESSAGES?                : Array<InfoMessageType>;
};

export enum AdvancedLoadingStatus {
	queued,
	loading,
	complete,
};
export type AdvancedLoaderType = {
	title: string,
	stages: Array<AdvancedLoaderStageType>
};
export type AdvancedLoaderStageType = {
	id: string,
	sortOrder: number,
	current?: number,
	total?: number,
	text: string,
	status: AdvancedLoadingStatus
};

export type InfoModalProps = {
	type: ModalTypes,
	data?: {
		title?  : string,
		text    : string | Array<string>,
		buttons?: Array<{
			text: string,
			clickFunc: Function,
		}>,
		links?: Array<{
			text: string,
			url : string,
		}>,
	}
}

export enum ModalTypes {
	loading = 0,
	info = 1,
	error = 2,
	success = 3,
}