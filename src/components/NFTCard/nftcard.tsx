import React, {
	useState
} from 'react';
import Tippy               from '@tippyjs/react';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import icon_logo           from "../../static/pics/logo.svg";
import icon_i_copy         from '../../static/pics/icons/i-copy.svg';
import icon_i_plus         from '../../static/pics/icons/i-plus.svg';
import icon_i_link         from '../../static/pics/icons/i-link.svg';
import icon_i_action       from '../../static/pics/icons/i-dots-hor.svg';

import { useAppSelector } from '../../redux/store';
import {
	BigNumber,
	Lock,
	NFTorWNFT,
	addThousandSeparator,
	assetTypeToString,
	compactString,
	tokenToFloat
} from 'envelop-client-core';
import { Link } from 'react-router-dom';
import { LockType, castToWNFT } from 'envelop-client-core/dist/_types';
import { unixtimeToStr } from '../../_utils';
import CoinIconViewer from '../CoinIconViewer';
import CoinViewer from '../CoinViewer';
import TransferPopup from '../TransferPopup';
import AddValuePopup from '../AddValuePopup';
import ApprovePopup from '../ApprovePopup';

export enum TokenRenderType {
	wnftv0,
	wrapped,
	discovered,
}

export default function NFTCard(params: { token: NFTorWNFT, tokenType: TokenRenderType }) {

	const v0AppLink = 'https://app.envelop.is';

	const { token, tokenType } = params;

	let   copiedHintTimer    = 0;
	const copiedHintTimeout  = 2; // s

	const userMenuBlockRef  : React.RefObject<HTMLDivElement> = React.createRef();
	const tokensListBlockRef: React.RefObject<HTMLDivElement> = React.createRef();

	const currentChain       = useAppSelector((state) => { return state.currentChain });

	const userAccount        = useAppSelector((state) => { return state.account });
	const erc20List          = useAppSelector((state) => { return state.erc20List });

	const [ copiedHintWhere , setCopiedHintWhere  ] = useState('');
	const [ menuOpened      , setMenuOpened       ] = useState(false);
	const [ cursorOnCardMenu, setCursorOnCardMenu ] = useState(false);
	const [ tokensListOpened, setTokensListOpened ] = useState(false);

	const [ addValuePopup   , setAddValuePopup ] = useState(false);
	const [ transferPopup   , setTransferPopup ] = useState(false);
	const [ approvePopup    , setApprovePopup  ] = useState(false);
	// const [ freezePopup     , setFreezePopup   ] = useState(false);

	if ( !currentChain ) { return null; }

	// this.state = {
	// 	minterContract       : this.store.getState().metamaskAdapter.minterContract,
	// 	crossing             : this.store.getState().metamaskAdapter.crossing,
	// }

	const getCopiedHint = (where: string) => {
		if ( copiedHintWhere === where  ) {
			return (<span className="btn-action-info">{ 'Copied' }</span>)
		}
	}
	const getCopyButton = () => {
		let tokenId = token.tokenId;
		let contractAddress = token.contractAddress;

		return (
			<CopyToClipboard
				text={ `${window.location.origin}/token/${currentChain.chainId}/${contractAddress}/${tokenId}` }
				onCopy={() => {
					setCopiedHintWhere('tokenlinkimg');
					clearTimeout(copiedHintTimer);
					copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere('') }, copiedHintTimeout*1000);
				}}
			>
				<button className="btn-token-link">
					<img src={ icon_i_link } alt="" />
					{ getCopiedHint('tokenlinkimg') }
				</button>
			</CopyToClipboard>
		)
	}

	// ----- HEADER -----
	const getIdBlock = () => {

		const tokenId = `${token.tokenId}`.length < 8 ? compactString(token.tokenId) : token.tokenId;

		return (
			<CopyToClipboard
				text={ tokenId }
				onCopy={() => {
					setCopiedHintWhere('id');
					clearTimeout(copiedHintTimer);
					copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
				}}
			>
				<button className="btn-copy">
					<Tippy
						content={ tokenId }
						appendTo={ document.getElementsByClassName("wrapper")[0] }
						trigger='mouseenter'
						interactive={ false }
						arrow={ false }
						maxWidth={ 512 }
					>
						<span>
							<span className="title">{ 'ID' }</span>
							{ tokenId }
						</span>
					</Tippy>
					<img src={icon_i_copy} alt="" />
					{ getCopiedHint('id') }
				</button>
			</CopyToClipboard>
		)
	}
	const getContractAddressBlock = () => {
		return (
			<CopyToClipboard
				text={ token.contractAddress }
				onCopy={() => {
					setCopiedHintWhere('address');
					clearTimeout(copiedHintTimer);
					copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere(''); }, copiedHintTimeout*1000);
				}}
			>
				<button className="btn-copy">
					<Tippy
						content={ token.contractAddress }
						appendTo={ document.getElementsByClassName("wrapper")[0] }
						trigger='mouseenter'
						interactive={ false }
						arrow={ false }
						maxWidth={ 512 }
					>
						<span>
							<span className="title">{ 'ADDRESS' }</span>
							{ compactString(token.contractAddress) }
						</span>
					</Tippy>
					<img src={icon_i_copy} alt="" />
					{ getCopiedHint('address') }
				</button>
			</CopyToClipboard>
		)
	}
	const getCardHeader = () => {
		return (
			<div>
				{ getContractAddressBlock() }
				{ getIdBlock() }
			</div>
		)
	}
	// ----- MENU -----
	const getMenuItemCopy = () => {
		return (
			<li>
				<CopyToClipboard
					text={ `${window.location.origin}/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}` }
					onCopy={() => {
						setCopiedHintWhere('tokenlinkimg');
						clearTimeout(copiedHintTimer);
						copiedHintTimer = window.setTimeout(() => { setCopiedHintWhere('') }, copiedHintTimeout*1000);
					}}
				>
					<button>
					{ 'Copy NFT URL' }
					</button>
				</CopyToClipboard>
			</li>
		)
	}
	const getMenuItemTransfer = () => {
		const getTransferFeeLabel = () => {
			if ( !token.fees || !token.fees.length ) { return null; }

			const foundToken = erc20List.find((item) => {
				if ( !token.fees ) { return false; }
				return item.contractAddress.toLowerCase() === token.fees[0].token.toLowerCase()
			});
			if ( foundToken ) {
				return (
					<span className="data">
						{ addThousandSeparator(tokenToFloat(token.fees[0].value, foundToken.decimals || 18).toString()) }
						{ '  ' }
						{ foundToken.symbol }
					</span>
				)
			}

		}

		if ( token.owner && token.owner.toLowerCase() !== userAccount?.userAddress.toLowerCase() ) { return null; }
		if ( token.amount && token.amount.lte(0) ) { return null; }

		return (
			<li>
				<button
					onClick={() => {
						setTransferPopup(true)
					}}
				>
					{ 'Transfer' }
					<span className="data">
						{ getTransferFeeLabel() }
					</span>
				</button>
			</li>
		)
	}
	const getMenuItemApprove = () => {
		return (
			<li>
				<button
					onClick={() => {
						setApprovePopup(true);
					}}
				>
					{ 'Set approval for all tokens of contract' }
				</button>
			</li>
		)
	}
	const getMenuTokenSellButton = () => {
		if ( !userAccount ) { return null; }
		if ( token.owner && token.owner.toLowerCase() !== userAccount.userAddress.toLowerCase() ) { return null; }

		return (
			<li>
				<button
					onClick={() => {
						if ( !token ) { return; }
						window.open(`https://scotch.sale/nft/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`, '_blank')
					}}
				>
					Sell
				</button>
			</li>
		)
	}
	// const getMenuItemCrossingPopup = () => {
	// 	if ( this.state.crossing ) {
	// 		return (
	// 			<li>
	// 				<button
	// 					onClick={() => {
	// 						this.setState({ freezePopup: true });
	// 					}}
	// 				>{ this.t('Prepare for crossing') }</button>
	// 			</li>
	// 		)
	// 	}
	// }
	const openCardMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !userMenuBlockRef.current ) { return; }
				if ( e.path && e.path.includes(userMenuBlockRef.current) ) { return; }
				closeCardMenu();
			};
		}, 100);
		setMenuOpened(true);
	}
	const closeCardMenu = () => {
		setTimeout(() => {
			if ( cursorOnCardMenu ) { return; }
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			setMenuOpened(false);
		}, 100);
	}
	const getCardMenuList = () => {
		if ( menuOpened ) {
			return (
				<ul
					className="list-action"
					onMouseEnter={() => {
						openCardMenu();
						setCursorOnCardMenu(true);
					}}
					onMouseLeave={() => {
						closeCardMenu();
						setCursorOnCardMenu(false);
					}}
				>
					{ getMenuItemCopy() }
					{ getMenuItemTransfer() }
					{ getMenuItemApprove() }
					{ getMenuTokenSellButton() }
					{/* { getMenuItemCrossingPopup() } */}
				</ul>
			)
		}
	}
	const getCardMenu = () => {
		return (
			<div
				className="w-card__action"
				ref={ userMenuBlockRef }
			>
				<button
					className="btn-action"
					onMouseEnter={ openCardMenu }
					onMouseLeave={ closeCardMenu }
					onClick={ openCardMenu }
				>
					<img src={icon_i_action} alt="" />
				</button>

				{ getCardMenuList() }
			</div>
		)
	}
	// ----- END MENU -----
	// ----- END HEADER -----

	// ----- DATA -----
	const closeTokenList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		setTokensListOpened(false);
	}
	const openTokenList = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = (e: any) => {
			if ( !tokensListBlockRef.current ) { return; }
			if ( e.path && e.path.includes(tokensListBlockRef.current) ) { return; }
			closeTokenList();
		};
		setTokensListOpened(true);
	}
	const getCollateralBlock = () => {

		if ( !token.collateral ) { return; }

		const ITEMS_TO_SHOW = 5;
		let additionalAssetssQty = token.collateral.length - ITEMS_TO_SHOW;

		return (
			<div className="field-wrap">
				<div className="field-row">
					<label className="field-label">{ 'Collateral' }</label>
				</div>
				<div className="field-row">
					<div
						className="field-control field-collateral"
						ref={ tokensListBlockRef }
						onClick={ openTokenList }
						onMouseEnter={ openTokenList }
						onMouseLeave={ closeTokenList }
					>
						<CoinIconViewer tokens = { token.collateral.slice(0,ITEMS_TO_SHOW) } />
						{
							token.collateral.length === 0 ?
							(
								<div className="sum">no assets</div>
							) : null
						}
						{
							token.collateral.length > ITEMS_TO_SHOW ?
							(
								<span className="more-assets">
									{ '+' }
									{ additionalAssetssQty || '' }
									{ ' ' }
									{ additionalAssetssQty > 0 ? ( additionalAssetssQty === 1 ? 'asset' : 'assets' ) : 'no assets' }
								</span>
							) : null
						}
						{
							tokensListOpened && token.collateral.length ?
								<CoinViewer tokens = { token.collateral } />
								: null
						}
					</div>
					{
						token.rules && !token.rules.noAddCollateral ?
						(
							<button
								className="btn-add"
								onClick={() => { setAddValuePopup(true); }}
							>
								<img src={ icon_i_plus } alt="" />
							</button>
						) : ''
					}
				</div>
			</div>
		)
	}
	// ----- END DATA -----

	const getType = () => {
		if ( !token.assetType ) { return null; }

		return ( <span className="w-card__tag" style={{ textTransform: 'none' }}>{ assetTypeToString(token.assetType, currentChain.EIPPrefix || 'ERC') }</span> )
	}
	// const getIsNFTKey = () => {
	// 	if ( !this.state.crossing || !this.state.crossing.spawner) { return null; }

	// 	if ( this.props.wrappedToken  && this.props.wrappedToken.contractAddress.toLowerCase()  !== this.state.crossing.spawner.toLowerCase() ) { return null; }
	// 	if ( this.props.originalToken && this.props.originalToken.contractAddress && this.props.originalToken.contractAddress.toLowerCase() !== this.state.crossing.spawner.toLowerCase() ) { return null; }

	// 	return ( <span className="w-card__tag bg-white">NFT key</span> )
	// }
	const getExtra = () => {
		return (
			<div className="extra">
				{ getType() }
				{/* { getIsNFTKey() } */}
				{ getCopyButton() }
			</div>
		)
	}
	const getMedia = () => {
		const image = token.image;
		const contractAddress = token.contractAddress;
		const tokenId = token.tokenId;

		if ( image === undefined ) {
			return (
				<Link
					className="inner"
					to={`/token/${currentChain.chainId}/${contractAddress}/${tokenId}`}
				>
					<div className="default">
						<img src={ icon_logo } alt="" />
						<span>Loading NON-FUNGIBLE <br /> TOKEN Preview</span>
					</div>
				</Link>
			)
		}
		if ( image === '' ) {
			return (
				<Link
					className="inner"
					to={`/token/${currentChain.chainId}/${contractAddress}/${tokenId}`}
				>
					<div className="default">
						<img src={ icon_logo } alt="" />
						<span>Cannot load NON-FUNGIBLE <br /> TOKEN Preview</span>
					</div>
				</Link>
			)
		}

		return (
			<Link
				className="inner"
				to={`/token/${currentChain.chainId}/${contractAddress}/${tokenId}`}
			>
				<video className="img" src={ image } poster={ image } autoPlay={ true } muted={ true } loop={ true } />
			</Link>
		)
	}
	const getCardFooter = () => {

		if ( tokenType === TokenRenderType.wnftv0 ) {
			return (
				<a
					className="btn btn-lg btn-yellow"
					target="_blank" rel="noopener noreferrer"
					href={`${v0AppLink}/#/token?chain=${currentChain.chainId}&contractAddress=${token.contractAddress}&tokenId=${token.tokenId}`}
				>{ 'Unwrap in v0 app' }</a>
			)
		}

		if ( tokenType === TokenRenderType.discovered ) {
			if ( !token ) { return null; }
			return (
				<Link
					className="btn btn-lg btn-primary"
					to={`/wrap/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
				>{ 'WRAP' }</Link>
			)
		}

		const conditions: Array<string> = [];

		if ( token.rules && token.rules.noUnwrap ) {
			conditions.push('No unwrap')
		} else {
			if ( token.locks ) {
				token.locks.forEach((item: Lock) => {

					if ( item.lockType === LockType.time ) {
						const nowDate = new BigNumber(new Date().getTime());
						if ( item.param.gt( nowDate ) ) { conditions.push(unixtimeToStr(item.param)) }
					}

					if ( item.lockType === LockType.value ) {

						if ( token.fees && token.fees.length && token.collectedFees ) {

							if ( item.param.gt(token.collectedFees) ) {

								let foundToken = erc20List.find((iitem) => {
									if ( !token || !token.fees ) { return false; }
									return iitem.contractAddress.toLowerCase() === token.fees[0].token.toLowerCase()
								});
								if ( !foundToken ) { return null; }

								const diff = tokenToFloat(item.param.plus(-token.collectedFees), foundToken.decimals || 18);
								conditions.push(`${addThousandSeparator(diff.toString())} more ${foundToken.symbol}`)
							}

						}
					}

				})
			}
		}

		if (conditions.length) {
			return (
				<div className="w-card__status">
					<div>
						<span className="small">{ 'Cannot unwrap token:' }</span>
						<span>{ conditions.join(', ') }</span>
					</div>
				</div>
			)
		}

		return (
			<Link
				className="btn btn-lg btn-yellow"
				to={`/token/${currentChain.chainId}/${token.contractAddress}/${token.tokenId}`}
			>{ 'UNWRAP' }</Link>
		)
	}

	// const getFreezePopup = () => {

	// 	if ( !this.state.freezePopup ) { return null; }
	// 	if ( this.props.wrappedToken ) {
	// 		return (
	// 			<CrossingFreezePopup
	// 				store={ this.store }
	// 				metamaskAdapter={ this.metamaskAdapter }
	// 				token={ this.props.wrappedToken }
	// 				closePopup={()=>{
	// 					this.setState({ freezePopup: false })
	// 				}}
	// 			/>
	// 		)
	// 	}
	// 	if ( this.props.originalToken ) {
	// 		return (
	// 			<CrossingFreezePopup
	// 				store={ this.store }
	// 				metamaskAdapter={ this.metamaskAdapter }
	// 				token={ this.props.originalToken }
	// 				closePopup={()=>{
	// 					this.setState({ freezePopup: false })
	// 				}}
	// 			/>
	// 		)
	// 	}
	// 	return null;
	// }

	return (
		<React.Fragment>
		<div className="w-card w-card-preview" key={`${token.contractAddress}${token.tokenId}`}>
			<div className="bg">
				<div className="w-card__meta">
					{ getCardHeader() }
					{ getCardMenu() }
				</div>

				<div className="w-card__token">
					{ getExtra() }
					{ getMedia() }
				</div>

				{
					tokenType === TokenRenderType.wrapped ?
					(
						<div className="w-card__param">
							<div className="form-row">
								{ getCollateralBlock() }
							</div>
							{/* <div className="form-row">
								{ getCollectedFeeBlock() }
								{ getTransferFeeBlock() }
								{ getRoyaltyBlock() }
							</div> */}
						</div>
					) : null
				}
				{ getCardFooter() }
			</div>
		</div>
		{
			params.token && addValuePopup ?
			(
				<AddValuePopup
					token={ castToWNFT(params.token) }
					closePopup={()=>{
						setAddValuePopup(false)
					}}
				/>
			)
			: null
		}
		{
			transferPopup ?
			(
				<TransferPopup
					token={ token }
					closePopup={()=>{
						setTransferPopup(false);
					}}
				/>
			)
			: null
		}
		{
			approvePopup ?
			(
				<ApprovePopup
					token={ token }
					closePopup={()=>{
						setApprovePopup(false);
					}}
				/>
			)
			: null
		}
		{/* { getFreezePopup() } */}
		</React.Fragment>
	)
}