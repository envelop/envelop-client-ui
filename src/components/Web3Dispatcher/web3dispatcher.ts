
import React from "react";

export type Web3DispatcherType = {
	get: Function,
	getForce: Function,
}

export const Web3Dispatcher = React.createContext<Web3DispatcherType>({
	get: Function,
	getForce: Function
});