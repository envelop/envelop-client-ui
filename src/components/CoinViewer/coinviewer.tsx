
import { MouseEventHandler } from 'react';
import Tippy from '@tippyjs/react';
import {
	useAppDispatch,
	useAppSelector
} from '../../redux/store';
import {
	erc20TokenUpdate
} from '../../redux/basicActions';
import default_icon from 'envelop-client-core/static/pics/coins/_default.svg';
import default_nft from 'envelop-client-core/static/pics/_default_nft.svg';

import {
	BigNumber,
	CollateralItem,
	ERC20Type,
	_AssetType,
	compactString,
	getERC20Params,
	tokenToFloat
} from 'envelop-client-core';

type CoinViewerProps = {
	tokens        : Array<CollateralItem>,
	position?     : string,
	onClick?      : MouseEventHandler<HTMLDivElement>,
	onMouseEnter? : MouseEventHandler<HTMLDivElement>,
	onMouseLeave? : MouseEventHandler<HTMLDivElement>,
}

export default function CoinViewer(props: CoinViewerProps) {

	const currentChain = useAppSelector((state) => { return state.currentChain });
	const erc20List    = useAppSelector((state) => { return state.erc20List });

	const dispatch     = useAppDispatch();

	const getCollateralItem = (item: CollateralItem) => {

		// native token
		if ( item.assetType === _AssetType.native && item.amount ) {
			return (
				<tr key={ 'native' }>
					<td>
						<span className="unit-sum">
							{ tokenToFloat(item.amount, currentChain?.decimals).toString() }
						</span>
					</td>
					<td>
						<span className="field-unit">
							<span className="i-coin">
								<img src={ currentChain?.tokenIcon || default_icon } alt="" />
							</span>
							{ currentChain?.symbol || compactString('0x0000000000000000000000000000000000000000') }
						</span>
					</td>
				</tr>
			)
		}

		if ( item.assetType === _AssetType.ERC20 && item.amount ) {
			// Common ERC20
			const foundERC20 = erc20List.filter((iitem: ERC20Type) => {
				if ( !iitem.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase()
			});
			if ( foundERC20.length ) {
				// known ERC20
				return (
					<tr key={ item.contractAddress }>
						<td>
							<span className="unit-sum">
								{ tokenToFloat(item.amount, foundERC20[0].decimals || 18).toString() }
							</span>
						</td>
						<td>
							<span className="field-unit">
								<span className="i-coin">
									<img src={ foundERC20[0].icon || default_icon } alt="" />
								</span>
								{
									currentChain ? (
										<a target="_blank" rel="noopener noreferrer" href={ `${currentChain.explorerBaseUrl}/token/${foundERC20[0].contractAddress}` }>
											{ foundERC20[0].symbol }
										</a>
									) : null
								}
							</span>
						</td>
					</tr>
				)
			} else {
				// unknown ERC20
				getERC20Params(currentChain?.chainId || 1, item.contractAddress)
					.then((data) => { console.log(data); if ( data ) { dispatch(erc20TokenUpdate(data)) } })
				return (
					<tr key={ item.contractAddress }>
						<td>
							<span className="unit-sum">
								<Tippy
									content={ 'decimals is unknown; amount shown in wei' }
									appendTo={ document.getElementsByClassName("wrapper")[0] }
									trigger='mouseenter'
									interactive={ false }
									arrow={ false }
									maxWidth={ 260 }
								>
									<span className="unit-sum">
										{ item.amount.toString() }*
									</span>
								</Tippy>
							</span>
						</td>
						<td>
							<span className="field-unit">
								<span className="i-coin">
									<img src={ default_icon } alt="" />
								</span>
								{
									currentChain ? (
										<a target="_blank" rel="noopener noreferrer" href={ `${currentChain.explorerBaseUrl}/token/${item.contractAddress}` }>
											{ compactString(item.contractAddress) }
										</a>
									) : null
								}
							</span>
						</td>
					</tr>
				)
			}
		}

		if ( item.assetType === _AssetType.ERC721 ) {
			return (
				<tr key={ `${item.contractAddress}${item.tokenId}` }>
					<td>
						<span className="unit-sum">
							<span className="unit-sum">
								{ item.amount && !item.amount.eq(0) ? item.amount.toString() : '1' } { ' ' } ({ currentChain?.EIPPrefix || 'ERC' }-721)
							</span>
						</span>
					</td>
					<td>
						<span className="field-unit">
							<span className="i-coin">
								<img src={ item.tokenImg || default_nft } alt="" />
							</span>
							{
								currentChain ? (
									<a target="_blank" rel="noopener noreferrer" href={ `${currentChain.explorerBaseUrl}/token/${item.contractAddress}/${item.tokenId || ''}` }>
										{ compactString(item.contractAddress) }
									</a>
								) : null
							}
						</span>
					</td>
				</tr>
			)
		}

		if ( item.assetType === _AssetType.ERC1155 ) {
			return (
				<tr key={ `${item.contractAddress}${item.tokenId}` }>
					<td>
						<span className="unit-sum">
							<span className="unit-sum">
								{ item.amount && !item.amount.eq(0) ? item.amount.toString() : '1' } { ' ' } ({ currentChain?.EIPPrefix || 'ERC' }-1155)
							</span>
						</span>
					</td>
					<td>
						<span className="field-unit">
							<span className="i-coin">
								<img src={ item.tokenImg || default_nft } alt="" />
							</span>
							{
								currentChain ? (
									<a target="_blank" rel="noopener noreferrer" href={ `${currentChain.explorerBaseUrl}/token/${item.contractAddress}/${item.tokenId || ''}` }>
										{ compactString(item.contractAddress) }
									</a>
								) : null
							}
						</span>
					</td>
				</tr>
			)
		}

		return null;
	}

	return (
		<div
			className    = { `field-collateral__details ${ props.position || '' }` }
			onClick      = { props.onClick }
			onMouseEnter = { props.onMouseEnter }
			onMouseLeave = { props.onMouseLeave }
		>
		<div className="inner">
			<table>
				<tbody>
					{
						props.tokens
							.sort((item, prev) => {
								if ( item.assetType < prev.assetType ) { return -1 }
								if ( item.assetType > prev.assetType ) { return  1 }

								if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
								if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

								if ( item.tokenId && prev.tokenId ) {
									try {
										if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
											if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return -1 }
											if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return  1 }
										}
										const itemTokenIdNumber = new BigNumber(item.tokenId);
										const prevTokenIdNumber = new BigNumber(prev.tokenId);

										if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return -1 }
										if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return  1 }
									} catch ( ignored ) {
										if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return -1 }
										if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return  1 }
									}
								}

								return 0
							})
							.map((item) => { return getCollateralItem(item) })
					}
				</tbody>
			</table>
		</div>
		</div>
	)

}