
import React, {
	useState
}  from 'react';
import {
	useAppDispatch,
	useAppSelector
} from '../../redux/store';
import { CopyToClipboard }  from 'react-copy-to-clipboard';
import Blockies             from 'react-blockies';
import { Link }             from 'react-router-dom';
import { Web3 }          from 'envelop-client-core';
import {
	localStorageRemove
} from '../../_utils';
import {
	compactString,
} from 'envelop-client-core';
import {
	requestChain, updateAccount,
} from '../../redux/basicActions';

import {
	getChainId,
	getNativeBalance,
	getUserAddress
} from 'envelop-client-core';

import icon_logo            from '../../static/pics/logo.svg';
import icon_logo_mob        from '../../static/pics/logo-mob.svg';
import icon_i_copy          from '../../static/pics/icons/i-copy.svg';
import icon_i_arrow_down    from '../../static/pics/icons/i-arrow-down.svg';
import default_icon         from 'envelop-client-core/static/pics/networks/_default.png';
import Web3Dispatcher, {
	Web3DispatcherType
} from '../Web3Dispatcher';

export default function Header() {

	const dispatch               = useAppDispatch();
	const currentChain           = useAppSelector((state) => { return state.currentChain });
	const userAccount            = useAppSelector((state) => { return state.account });
	const availableChains        = useAppSelector((state) => { return state.availableChains });
	const versionMenuBlockRef    = React.createRef<HTMLDivElement>();
	const chainMenuBlockRef      = React.createRef<HTMLDivElement>();
	const userMenuBlockRef       = React.createRef<HTMLDivElement>();

	const [ copiedHint       , setCopiedHint        ] = useState(false);
	const [ versionMenuOpened, setVersionMenuOpened ] = useState(false);
	const [ chainMenuOpened  , setChainMenuOpened   ] = useState(false);
	const [ userMenuOpened   , setUserMenuOpened    ] = useState(false);
	const [ cursorOnUserMenu , setCursorOnUserMenu  ] = useState(false);

	const getLogo = () =>  {
		return (
			<React.Fragment>
				<Link
					to="/"
					className="s-header__logo d-none d-sm-block"
				>
					<img src={ icon_logo } alt="ENVELOP" />
				</Link>
				<Link
					to="/"
					className="s-header__logo mob d-sm-none"
				>
					<img src={ icon_logo_mob } alt="ENVELOP" />
				</Link>
			</React.Fragment>
		)
	}
	const getVersionBlock = () =>  {
		const closeVersionMenu = () => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			setVersionMenuOpened(false);
		}
		const openVersionMenu = () => {
			setTimeout(() => {
				const body = document.querySelector('body');
				if ( !body ) { return; }
				body.onclick = (e: any) => {
					if ( !versionMenuBlockRef.current ) { return; }
					if ( e.path && e.path.includes(versionMenuBlockRef.current) ) { return; }
					closeVersionMenu();
				};
			}, 100);
			setVersionMenuOpened(true);
		}
		return (
			<div
				className="s-header__version"
				ref={ versionMenuBlockRef }
				onMouseLeave={ closeVersionMenu }
			>
				<button
					className={ `btn btn-sm btn-gray ${ versionMenuOpened ? 'active' : '' }` }
					onClick={ openVersionMenu }
					onMouseEnter={ openVersionMenu }
				>
					<span>v.1.0</span>
					<img className="arrow" src={ icon_i_arrow_down } alt="" />
				</button>
				{
					versionMenuOpened ?
					(
						<div className="btn-dropdown">
							<ul>
								<li>
									<button onClick={() => { window.location.href = 'https://app.envelop.is' }} className="item">v.0</button>
								</li>
								<li>
									<button className="item">v.1.0</button>
								</li>
							</ul>
						</div>
					) : null
				}
			</div>
		)
	}
	const getNavLinks = () =>  {
		return (
			<div className="s-header__nav">
				<div className="container">
					<div className="nav-item">
						<a className="link" target="_blank" rel="noopener noreferrer" href="https://app.envelop.is/farming/">Farming</a>
					</div>
					<div className="nav-item">
						<div className="link">
							<span className="mr-1">Launchpads</span>
							<svg className="arrow" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z" fill="white"></path>
							</svg>
						</div>
						<div className="nav-dropdown dropdown-launchpads">
							<div className="dropdown-item">
								<a target="_blank" rel="noopener noreferrer" href="https://christmas.envelop.is/">
									<span>Christmas 2022</span>
									<small>Envelop presents a Christmas2022 collection</small>
								</a>
							</div>
							<div className="dropdown-item">
								<a target="_blank" rel="noopener noreferrer" href="https://halloween.envelop.is/">
									<span>Halloween 2022</span>
									<small>Collection "All Saints' NFT"</small>
								</a>
							</div>
						</div>
					</div>
					<div className="nav-item">
						<a href={`${window.location.origin}/mint`} className="link">Mint</a>
					</div>
					<div className="nav-item">
						<Link className="link" to="/saft">SAFT</Link>
					</div>
					{/* <div className="nav-item">
						<div className="link">
							<svg className="mr-1" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M17 3H7C5.9 3 5.01 3.9 5.01 5L5 21L12 18L19 21V5C19 3.9 18.1 3 17 3ZM17 18L12 15.82L7 18V5H17V18Z" fill="#4AFEBF"></path>
							</svg>
							<span className="mr-1">My Services</span>
							<svg className="arrow" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z" fill="white"></path>
							</svg>
						</div>
						<div className="nav-dropdown dropdown-services">
							<div className="row">
								<div className="col-12 col-md-6 mb-4">
									<div className="mb-2">
										<b className="text-green2">Farms </b>
									</div>
									<div className="dropdown-item">
										<a href="#">
											<span>Christmas Farm Promo</span>
										</a>
									</div>
									<a className="btn btn-sm btn-border" href="#">Create a farm</a>
								</div>
								<div className="col-12 col-md-6">
									<div className="mb-2">
										<b className="text-green2">Launchpads</b>
									</div>
									<div className="dropdown-item">
										<a href="#">
											<span>Christmas Collection</span>
										</a>
									</div>
									<div className="dropdown-item">
										<a href="#">
											<span>Autumn Dreams</span>
										</a>
									</div>
									<a className="btn btn-sm btn-border" href="#">Create a launchpad</a>
								</div>
							</div>
						</div>
					</div> */}
				</div>
			</div>
		)
	}
	const getChainSelectorDropdown = () =>  {
		if ( !chainMenuOpened ) { return null; }

		return (
			<div className="btn-dropdown">
			<ul>
				{
					availableChains.map((item) => {

						return (
							<li key={ item.chainId }>
								<button
									className={`item`}
									onClick={() => { dispatch(requestChain(item.chainId)) }}
								>
									<span className="logo"><img src={item.networkIcon} alt="" /></span>
									<span className="name">{ `${item.name} ${item.isTestNetwork ? 'Testnet' : ''}` }</span>
								</button>
							</li>
						)
					})
				}
			</ul>
			</div>
		)
	}
	const getChainSelector = () =>  {

		if ( !currentChain ) { return null; }

		const closeChainMenu = () => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			setChainMenuOpened(false);
		}
		const openChainMenu = () => {
			setTimeout(() => {
				const body = document.querySelector('body');
				if ( !body ) { return; }
				body.onclick = (e: any) => {
					if ( !chainMenuBlockRef.current ) { return; }
					if ( e.path && e.path.includes(chainMenuBlockRef.current) ) { return; }
					closeChainMenu();
				};
			}, 100);
			setChainMenuOpened(true);
		}
		return (
			<div
				className={`s-header__network ${ currentChain.isTestNetwork ? 'test-network' : ''}`}
				ref={ chainMenuBlockRef }
				onMouseLeave={ closeChainMenu }
			>
				<button
					className={ `btn btn-sm btn-gray btn-network ${ chainMenuOpened ? 'active' : '' }`}
					onClick={ openChainMenu }
					onMouseEnter={ openChainMenu }
				>
					<span className="logo">
						<img src={ currentChain.networkIcon || default_icon } alt="" />
					</span>
					<span className="name">{ `${currentChain.name} ${currentChain.isTestNetwork ? 'Testnet' : ''}` }</span>
					<svg className="arrow" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z" fill="white"></path>
					</svg>
				</button>

				{ getChainSelectorDropdown() }
			</div>
		)
	}
	const getConnectBtn = (web3dispatcher: Web3DispatcherType) => {
		return (
			<button
				className="btn btn-connect"
				onClick={async (e) => {
					let web3ToUse: Web3 = await web3dispatcher.getForce();
					if ( !web3ToUse ) { return; }
					getUserAddress(web3ToUse)
						.then(async (data) => {
							if ( !data ) { return; }
							dispatch(updateAccount({ userAddress: data }));
							const chainId = await getChainId(web3ToUse);
							if ( !chainId ) { return; }
							getNativeBalance(chainId, data).then((ddata) => { dispatch(updateAccount({ balanceNative: ddata })) });
						});
				}}
			>
				Connect
				<span className="d-none d-md-inline">&nbsp;Wallet</span>
			</button>
		)
	}
	// const getBalancesFull = () =>  {
	// 	if ( !userAccount ) { return; }
	// 	const balances = [];

	// 	if ( currentChain.networkTokenDecimals ) {
	// 		balances.push(`${ tokenToFloat(new BigNumber(userAccount.balanceNative), currentChain.networkTokenDecimals) } ${ currentChain.networkTokenTicket }`)
	// 	}
	// 	return (
	// 		<div className="info">
	// 			{ balances.join(', ') }
	// 		</div>
	// 	)
	// }
	// const getBalancesShort = () =>  {
	// 	if ( !userAccount ) { return; }
	// 	const balances = [];

	// 	if ( currentChain.networkTokenDecimals ) {
	// 		balances.push(`${ tokenToFloat(new BigNumber(userAccount.balanceNative), currentChain.networkTokenDecimals).toFixed(3, BigNumber.ROUND_DOWN) } ${ currentChain.networkTokenTicket }`)
	// 	}
	// 	return (
	// 		<div className="info">
	// 			{ balances.join(', ') }
	// 		</div>
	// 	)
	// }
	// const getBalanceBlock = () =>  {
	// 	return (
	// 		<Tippy
	// 			content={ getBalancesFull() }
	// 			appendTo={ document.getElementsByClassName("wrapper")[0] }
	// 			trigger='mouseenter'
	// 			interactive={ false }
	// 			arrow={ false }
	// 			maxWidth={ 512 }
	// 		>
	// 			<div className="info">{ getBalancesShort() }</div>
	// 		</Tippy>
	// 	)
	// }

	const closeUserMenu = () => {
		setTimeout(() => {
			if ( cursorOnUserMenu ) { return; }
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = null;
			setUserMenuOpened(false);
		}, 100);
	}
	const openUserMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				if ( !userMenuBlockRef.current ) { return; }
				if ( e.path && e.path.includes(userMenuBlockRef.current) ) { return; }
				closeUserMenu();
			};
		}, 100);
		setUserMenuOpened(true);
	}
	const getAvatarBlock = () => {
		if ( !userAccount ) { return null; }
		return (
			<div className="s-user__avatar">
				<div className="img">
					<Blockies
						seed      = { userAccount.userAddress }
						size      = {5}
						scale     = {10}
						color     = "#141616"
						bgColor   = "#4afebf"
						spotColor = "#ffffff"
					/>
				</div>
			</div>
		)
	}
	const getUserMenu = () =>  {
		if ( !userMenuOpened ) { return; }

		return (
			<div
				className="s-user__menu"
				onMouseEnter={ () => {
					setCursorOnUserMenu(true);
					openUserMenu();
				}}
				onMouseLeave={ () => {
					setCursorOnUserMenu(false);
					closeUserMenu();
				}}
			>
				<ul className="inner">
					<li className="d-md-none">
						<div className="item address">
							<button className="btn-copy">
								<span>{ userAccount?.userAddress ? compactString(userAccount?.userAddress) : '' }</span>
								<img src={ icon_i_copy } alt="" />
								<span className="btn-action-info" style={{ display: copiedHint ? 'block' : 'none' }}>Copied</span>
							</button>
						</div>
					</li>
					<li><a href="/list"      className="item">Dashboard   </a></li>
					<li><a href="/crossings" className="item">My crossings</a></li>
					<li><a href="/royalty"   className="item">My Royalties</a></li>
					<li><a href="/mint"      className="item">Mint        </a></li>
					<li><a href="/saft"      className="item">SAFT        </a></li>

					<li className="mt-md-2">
						<button
							onClick={(e) => {
								localStorageRemove('authMethod');
								window.location.reload();
							}}
							className="item disconnect"
						>Disconnect</button>
					</li>
				</ul>
			</div>
		)
	}
	const getUserData = () =>  {
		if ( !userAccount ) { return null; }
		return (
			<React.Fragment>

				<div
					className="s-user"
					ref={ userMenuBlockRef }
					onClick={ openUserMenu }
					onMouseEnter={ openUserMenu }
					onMouseLeave={ closeUserMenu }
				>
					<div className="s-user__toggle">
						{ getAvatarBlock() }
						<div className="s-user__data">
							<span className="mr-2">{ compactString(userAccount.userAddress) }</span>
							<svg className="arrow" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z" fill="white" fillOpacity="0.6"></path>
							</svg>
						</div>
					</div>
					<CopyToClipboard
						text={ userAccount.userAddress }
						onCopy={() => {
							setCopiedHint(true);
							setTimeout(() => { setCopiedHint(false); }, 5*1000);
						}}
					>
						<button className="btn-copy">
							<img src={ icon_i_copy } alt="" />
							<span className="btn-action-info" style={{ display: copiedHint ? 'block' : 'none' }}>Copied</span>
						</button>
					</CopyToClipboard>
				</div>
				{ getUserMenu() }
			</React.Fragment>
		)
	}
	const getBtnOrData = (web3dispatcher: Web3DispatcherType) =>  {
		if ( !userAccount ) {
			return getConnectBtn(web3dispatcher)
		} else {
			return getUserData()
		}
	}

	return (
		<Web3Dispatcher.Consumer>
			{ web3dispatcher => (
				<header className="s-header">
					<div className="container-fluid">
						<div className="d-flex align-items-center h-100">
							{ getLogo() }
							{ getVersionBlock() }

							<button className="s-header__nav-toggle"><span className="burger-lines"></span></button>

							{ getNavLinks() }
						</div>

						<div className="d-flex align-items-center">
							{ getChainSelector() }
							{ getBtnOrData(web3dispatcher) }
						</div>
					</div>
				</header>
			)}
		</Web3Dispatcher.Consumer>
	)
}