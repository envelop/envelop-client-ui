
import { useState } from 'react';

type InputWithOptionsProps = {
	onChange     : (e: any) => void,
	onKeyPress?  : (e: any) => void,
	onSelect?    : (e: any) => void,
	inputClass?  : string,
	blockClass?  : string,
	readonly?    : boolean,
	disabled?    : boolean,
	value        : string,
	placeholder? : string,
	options?     : Array<{
		label: string,
		value: string
	}>
}
type InputWithOptionsState = {
	listOpened: boolean,
}

export default function InputWithOptions(props: InputWithOptionsProps) {

	const [ listOpened, setListOpened ] = useState(false);

	const getOptions = () => {
		if ( !props.options || !props.onSelect || !props.options.length ) { return null; }
		if ( !listOpened ) { return null; }

		return (
			<ul className="options-list">
				{
					props.options.map((item) => {
						return (
							<li
								key={ item.value }
								className="option"
								onClick={() => {
									if ( props.onSelect ) {
										props.onSelect(item.value)
										setListOpened(false);
									}
								}}
							>
								<div className="option-token">
									<span>{ item.label }</span>
								</div>
							</li>
						)
					})
				}
			</ul>
		)
	}

	let inputClazz = `input-control no-arrow ${ props.inputClass || ''} ${ listOpened ? 'active' : '' }`;
	let blockClazz = `select-custom select-token`;

	return (
		<div className = { blockClazz }>
			<input
				className   = { inputClazz }
				type        = "text"
				placeholder = { props.placeholder || '' }
				value       = { props.value }
				readOnly    = { props.readonly    || false }
				disabled    = { props.disabled    || false }
				onChange    = { props.onChange }
				onKeyPress  = { props.onKeyPress }
				onFocus     = { () => {
					if ( !props.options || !props.onSelect || !props.options.length ) { return; }
					setListOpened(true);
				}}
				onBlur      = { () => {
					setTimeout(() => {
						setListOpened(false);
					}, 200)
				}}
			/>
			{ getOptions() }
		</div>
	)
}