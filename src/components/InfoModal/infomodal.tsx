
import icon_envelop        from '../../static/pics/envelop.svg';
import icon_error          from '../../static/pics/img-error.svg';
import i_external_green_sm from '../../static/pics/icons/i-external-green-sm.svg';
import {
	InfoModalProps,
	ModalTypes
} from '../../_types';

const getIcon = (props: InfoModalProps) => {
	return (
		<img className="c-success__img" src={ props.type === ModalTypes.error ? icon_error : icon_envelop } alt="" />
	)
}
const getTitle = (props: InfoModalProps) => {
	if ( props.type === ModalTypes.loading ) {
		return (
			<div className="h2">
				Loading
				<span className="loading-dots"><span>.</span><span>.</span><span>.</span></span>
			</div>
		)
	}

	if ( props.type === ModalTypes.error ) {
		return (
			<div className="h2">{ props.data?.title || 'Error' }</div>
		)
	}

	if ( props.data?.title ) {
		return (
			<div className="h2">{ props.data?.title || '' }</div>
		)
	}

	return null;
}
const getText = (props: InfoModalProps) => {
	if ( props.data?.text ) {
		if ( props.data.text instanceof Array ) {
			return ( props.data.text.map((item) => { return (<p>{ item }</p>) }) )
		}

		return ( <p>{ props.data.text }</p> )
	}

	return null;
}

const getLinks = (props: InfoModalProps) => {
	if (props.data?.links && props.data.links.length ) {
		return props.data.links.map((item) => {
			return (
				<a
					className="ex-link mr-3"
					key={ item.url }
					href={ item.url }
					target="_blank" rel="noopener noreferrer"
				>
					{ item.text }
					<img className="i-ex" src={ i_external_green_sm } alt="" />
				</a>
			)
		})
	}
}

const getButtons = (props: InfoModalProps) => {

	if ( props.data?.buttons && props.data.buttons.length ) {
		return props.data.buttons.map((item, idx) => {
			let btnClass = '';
			if ( props.data && props.data.buttons && idx + 1 === props.data.buttons.length ) {
				btnClass = 'btn btn-outline'
			} else {
				btnClass = 'btn btn-grad'
			}
			return (
				<div
					className="col-12 col-sm-auto mb-3 mb-md-0"
					key={ item.text }
				>
					<button
						className={ btnClass }
						onClick={() => {
							item.clickFunc();
						}}
					>{ item.text }</button>
				</div>
			)
		})
	}

	return null;
}

export default function InfoModal(props: InfoModalProps) {

	return (
		<div className="modal">
		<div className="modal__inner">
		<div className="modal__bg"></div>
		<div className="container">
		<div className="modal__content">
			<div className="c-success">
				{ getIcon(props)    }
				{ getTitle(props)   }
				{ getText(props)    }
				{ getLinks(props)   }
				{ getButtons(props) }
			</div>
		</div>
		</div>
		</div>
		</div>
	)
}