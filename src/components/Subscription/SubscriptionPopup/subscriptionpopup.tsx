
import React, { useState } from 'react';
import Tippy from '@tippyjs/react';
import {
	useAppDispatch,
	useAppSelector
} from '../../../redux/store';
import {
	BigNumber,
	ERC20Type,
	SubscriptionRemainings,
	SubscriptionTariff,
	SubscriptionType,
	buySubscription,
	compactString,
	getERC20Params,
	getPriceWithFee,
	getTariffsForService,
	getUserTicketForService,
	tokenToFloat
} from 'envelop-client-core';
import { erc20TokenUpdate } from '../../../redux/basicActions';
import Web3Dispatcher from '../../Web3Dispatcher';
import default_icon   from '../../static/pics/networks/_default.png';
import { timeToDays } from '../../../_utils';

type SubscriptionRendererProps = {
	registryContract: string,
	agentContract   : string,
	serviceContract : string,
	TX_names        : { singular: string, plural: string },
	closePopup      : () => void,
}

export default function SubscriptionRenderer(props: SubscriptionRendererProps) {

	const {
		TX_names,
		closePopup,
		registryContract,
		agentContract,
		serviceContract,
	} = props;

	const currentChain = useAppSelector((state) => { return state.currentChain });
	const userAccount  = useAppSelector((state) => { return state.account      });
	const erc20List    = useAppSelector((state) => { return state.erc20List    });

	const dispatch = useAppDispatch();

	const [ subscriptionRemainings, setSubscriptionRemainings                  ] = useState<SubscriptionRemainings | undefined>(undefined);
	const [ paymentTokens,          setPaymentTokens                           ] = useState<Array<ERC20Type>>([]);
	const [ subscriptionTariffs,    setSubscriptionTariffs                     ] = useState<Array<SubscriptionTariff>>([]);

	const [ buyFor, setBuyFor                                                   ] = useState('');
	const [ subscriptionPaymentTypeSelected, setSubscriptionPaymentTypeSelected ] = useState('');
	const [ subscriptionTypeSelected, setSubscriptionTypeSelected               ] = useState('');
	const [ subscriptionPlanSelected, setSubscriptionPlanSelected               ] = useState('');
	const [ subscriptionTariffSelectedIdx, setSubscriptionTariffSelectedIdx     ] = useState(-1);
	const [ subscriptionTokenSelected, setSubscriptionTokenSelected             ] = useState('');
	const [ recieverEdited, setRecieverEdited                                   ] = useState(false);

	const [ priceWithFee, setPriceWithFee                                       ] = useState<BigNumber | undefined>(undefined)

	const fetchTariffs = () => {
		if ( !currentChain ) { return; }

		getTariffsForService(
			currentChain.chainId,
			registryContract,
			agentContract,
			serviceContract,
		)
			.then((data: Array<SubscriptionTariff>) => {
				if ( !data ) { return; }

				data.forEach((item) => {
					item.payWith.forEach((iitem) => {
						getERC20Params(currentChain.chainId, iitem.paymentToken, userAccount?.userAddress, registryContract)
							.then((ddata) => {
								setPaymentTokens([
									...paymentTokens
										.filter((iiitem) => { return iiitem.contractAddress.toLowerCase() !== ddata.contractAddress.toLowerCase() }),
									ddata
								])
							})
					})
				})

				setSubscriptionTariffs(data);
			})
	}
	const fetchTicket = () => {
		if ( !currentChain ) { return; }

		getUserTicketForService(
			currentChain.chainId,
			registryContract,
			agentContract,
			serviceContract,
		)
			.then((data: SubscriptionRemainings | undefined) => {
				if ( !data ) { return; }

				setSubscriptionRemainings(data)
			})
	}

	const getSubscriptionERC20Balance = () => {

		const foundToken = paymentTokens.find((item) => { return item.contractAddress.toLowerCase() === subscriptionTokenSelected.toLowerCase() });
		if ( !foundToken ) { return null; }

		const foundAllowance = foundToken.allowance.find((item) => { return item.allowanceTo.toLowerCase() === registryContract.toLowerCase(); })

		return (
			<div className="c-add__max">
				<span>Max: </span>
				<button>{ tokenToFloat(foundToken.balance, foundToken.decimals || 18).toString() }</button>
				{
					foundAllowance ? (
						<React.Fragment>
							<span className="ml-2">Allowance: </span>
							<button>{ tokenToFloat(foundAllowance.amount, foundToken.decimals || 18).toString() }</button>
						</React.Fragment>
					) : null
				}
			</div>
		)
	}

	const getPaymentToken = (address: string) => {
		if ( !currentChain ) { return undefined; }
		if ( !userAccount ) { return undefined; }

		if ( address === '0x0000000000000000000000000000000000000000' ) {
			return {
				name: currentChain.name,
				symbol: currentChain.symbol,
				decimals: currentChain.decimals,
				balance: userAccount.balanceNative,
				allowance: new BigNumber(0),
				icon: currentChain.networkIcon || default_icon,
				permissions: {}
			}
		}
		const foundToken = paymentTokens.find((item) => { return item.contractAddress.toLowerCase() === address.toLowerCase() });
		if ( !foundToken ) {
			return undefined;
		}

		return foundToken;
	}
	const getTicketDetails = () => {
		if ( subscriptionTariffSelectedIdx === -1 ) { return null; }

		const foundTicket = subscriptionTariffs.find((item) => { return item.idx === subscriptionTariffSelectedIdx });
		if ( !foundTicket ) { return null; }

		let fullPriceStr = undefined;
		const foundPayOption = foundTicket.payWith.find((item) => { return subscriptionTokenSelected.toLowerCase() === item.paymentToken.toLowerCase() })
		if ( foundPayOption ) {
			const payToken = getPaymentToken(foundPayOption.paymentToken);
			const symbolParsed = payToken ? payToken.symbol : foundPayOption.paymentToken;

			const decimalsParsed = payToken ? payToken.decimals : 18;
			if ( priceWithFee ) {
				fullPriceStr = `${tokenToFloat(priceWithFee, decimalsParsed)} ${symbolParsed}`
			}
		}

		fetchTariffs();
		fetchTicket();

		return (
			<React.Fragment>
			{
				!foundTicket.subscription.timelockPeriod.eq(0) ? (
					<p className="mt-0">
						Lock time:
						<br />
						<b className="text-green">
							{ timeToDays(foundTicket.subscription.timelockPeriod) }
							<Tippy
								content={ 'Time to lock your collateral tokens.' }
								appendTo={ document.getElementsByClassName("wrapper")[0] }
								trigger='mouseenter'
								interactive={ false }
								arrow={ false }
								maxWidth={ 512 }
							>
								<span className="i-tip ml-1"></span>
							</Tippy>
						</b>
					</p>
				) : null
			}
			{
				!foundTicket.subscription.ticketValidPeriod.eq(0) ? (
					<p className="mt-0">
						Subscription time:
						<br />
						<b className="text-green">
							{ timeToDays(foundTicket.subscription.ticketValidPeriod) }
							<Tippy
								content={ 'The period of time during which the subscription is valid. There is no limit on the number of times the service can be used.' }
								appendTo={ document.getElementsByClassName("wrapper")[0] }
								trigger='mouseenter'
								interactive={ false }
								arrow={ false }
								maxWidth={ 512 }
							>
								<span className="i-tip ml-1"></span>
							</Tippy>
						</b>
					</p>
				) : null
			}
			{
				parseInt(`${foundTicket.subscription.counter}`) !== 0 ? (
					<p className="mt-0">
						{ TX_names.plural.replace(/\w\S*/g, (txt) => { return txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase() }) } count:
						<b className="text-green">
							{ ' ' }
							{ foundTicket.subscription.counter }
							<Tippy
								content={ 'The number of calls to the service according to the price plan. There is no time limit.' }
								appendTo={ document.getElementsByClassName("wrapper")[0] }
								trigger='mouseenter'
								interactive={ false }
								arrow={ false }
								maxWidth={ 512 }
							>
								<span className="i-tip ml-1"></span>
							</Tippy>
						</b>
					</p>
				) : null
			}
			{
				fullPriceStr ? (
					<p className="mt-0">
						Price:
						<br />
						<b className="text-green">
							{ fullPriceStr }
							<Tippy
								content={ subscriptionPaymentTypeSelected === 'timelock' ? 'Number of collateral tokens that will be locked.' : 'Price with fee' }
								appendTo={ document.getElementsByClassName("wrapper")[0] }
								trigger='mouseenter'
								interactive={ false }
								arrow={ false }
								maxWidth={ 512 }
							>
								<span className="i-tip ml-1"></span>
							</Tippy>
						</b>
					</p>
				) : null
			}
			</React.Fragment>
		)
	}
	const getPaymentTypeSelect = () => {
		return (
			<div className="input-group">
				<label className="input-label">Payment type</label>
				<select
					className="input-control"
					value={ subscriptionPaymentTypeSelected }
					onChange={(e) => {

						setSubscriptionPaymentTypeSelected(e.target.value);
						setSubscriptionTypeSelected('');
						setSubscriptionPlanSelected('');
						setSubscriptionTariffSelectedIdx(-1);
						setSubscriptionTokenSelected('');
						setPriceWithFee(undefined);

						return;
					}}
				>
					<option value={ '' }>Choose payment type </option>
					<option value={ 'timelock' }>Token deposit</option>
					<option value={ 'buy' }>Direct payment</option>
				</select>
			</div>
		)
	}
	const getSubscriptionTypeSelect = () => {
		if ( subscriptionPaymentTypeSelected === '' ) { return null; }

		const subscriptionsFilteredTime = subscriptionTariffs
			.filter((item) => {
				if ( subscriptionPaymentTypeSelected === 'timelock' ) {
					return !item.subscription.timelockPeriod.eq(0)
				} else {
					return item.subscription.timelockPeriod.eq(0)
				}
			})
			.filter((item) => {
				return parseInt(`${item.subscription.counter}`) === 0
			});
		const subscriptionsFilteredTX = subscriptionTariffs
			.filter((item) => {
				if ( subscriptionPaymentTypeSelected === 'timelock' ) {
					return !item.subscription.timelockPeriod.eq(0)
				} else {
					return item.subscription.timelockPeriod.eq(0)
				}
			})
			.filter((item) => {
				return parseInt(`${item.subscription.counter}`) !== 0
			});

		return (
			<div className="input-group">
				<label className="input-label">Subscription type</label>
				<select
					className="input-control"
					value={ subscriptionTypeSelected }
					onChange={(e) => {

						setSubscriptionTypeSelected(e.target.value)
						setSubscriptionPlanSelected('')
						setSubscriptionTariffSelectedIdx(-1)
						setSubscriptionTokenSelected('')
						setPriceWithFee(undefined)

						return;
					}}
				>
					<option value={ '' }>Choose subscription type</option>
					{
						subscriptionsFilteredTime.length ? (
							<option value={ 'time' }>Period of services</option>
						) : null
					}
					{
						subscriptionsFilteredTX.length ? (
							<option value={ 'txs' }>Volume of services</option>
						) : null
					}
				</select>
			</div>
		)
	}
	const getSubscriptionPlanSelect = () => {
		if ( subscriptionTypeSelected === '' ) { return null; }

		const subscriptionsFiltered = subscriptionTariffs
			.filter((item) => {
				if ( subscriptionPaymentTypeSelected === 'timelock' ) {
					return !item.subscription.timelockPeriod.eq(0)
				} else {
					return item.subscription.timelockPeriod.eq(0)
				}
			})
			.filter((item) => {
				if ( subscriptionTypeSelected === 'time' ) {
					return parseInt(`${item.subscription.counter}`) === 0
				} else {
					return parseInt(`${item.subscription.counter}`) !== 0
				}
			})
			.sort((item, prev) => {
				const itemCount = parseInt(`${item.subscription.counter}`);
				const prevCount = parseInt(`${prev.subscription.counter}`);
				if ( itemCount > 0 && prevCount > 0 ) {
					return itemCount - prevCount
				}
				if ( item.subscription.ticketValidPeriod.gt(0) && prev.subscription.ticketValidPeriod.gt(0) ) {
					return item.subscription.ticketValidPeriod.plus(-prev.subscription.ticketValidPeriod).toNumber()
				}

				return 0;
			})

		return (
			<div className="input-group">
				<label className="input-label">Subscription plan</label>
				<select
					className="input-control"
					value={ subscriptionTariffSelectedIdx }
					onChange={(e) => {
						if ( subscriptionTokenSelected !== '' ) {

							const tariffSelected = subscriptionTariffs.find((item) => { return item.idx === parseInt(e.target.value) });
							if ( !tariffSelected ) { return; }
							const payOptionSelected = tariffSelected.payWith.find((item) => { return item.paymentToken.toLowerCase() === subscriptionTokenSelected.toLowerCase() })
							if ( !payOptionSelected ) { return; }

							getPriceWithFee(
								currentChain?.chainId || 1,
								registryContract,
								serviceContract,
								tariffSelected.idx,
								payOptionSelected.idx,
							)
							.then((data) => {
								setPriceWithFee(data);
							});
						}

						setSubscriptionTariffSelectedIdx(parseInt(e.target.value));
						setPriceWithFee(undefined);

						return;
					}}
				>
					<option value={ -1 }>Choose subscription plan</option>
					{
						subscriptionsFiltered.map((item) => {
							if ( parseInt(`${item.subscription.counter}`) !== 0 ) {
								return (
									<option value={ item.idx }>{ item.subscription.counter } { item.subscription.counter === 1 ? TX_names.singular : TX_names.plural }</option>
								)
							} else {
								return (
									<option value={ item.idx }>{ timeToDays(item.subscription.ticketValidPeriod) }</option>
								)
							}
						})
					}
				</select>
			</div>
		)
	}
	const getSubscriptionTokenSelect = () => {
		if ( subscriptionTariffSelectedIdx === -1 ) { return null; }

		const subscriptionsFound = subscriptionTariffs
			.filter((item) => {
				if ( subscriptionPaymentTypeSelected === 'timelock' ) {
					return !item.subscription.timelockPeriod.eq(0)
				} else {
					return item.subscription.timelockPeriod.eq(0)
				}
			})
			.filter((item) => {
				if ( subscriptionTypeSelected === 'time' ) {
					return parseInt(`${item.subscription.counter}`) === 0
				} else {
					return parseInt(`${item.subscription.counter}`) !== 0
				}
			})
			.find((item) => { return item.idx === subscriptionTariffSelectedIdx });

		return (
			<div className="input-group mb-0">
				<label className="input-label">Payment token</label>
				<select
					className="input-control"
					value={ subscriptionTokenSelected }
					onChange={(e) => {

						const tariffSelected = subscriptionTariffs.find((item) => { return item.idx === subscriptionTariffSelectedIdx });
						if ( !tariffSelected ) { return; }
						const payOptionSelected = tariffSelected.payWith.find((item) => { return item.paymentToken.toLowerCase() ===  e.target.value.toLowerCase() })
						if ( !payOptionSelected ) { return; }

						getPriceWithFee(
							currentChain?.chainId || 1,
							registryContract,
							serviceContract,
							tariffSelected.idx,
							payOptionSelected.idx,
						)
						.then((data) => {
							setPriceWithFee(data);
						});

						setSubscriptionTariffSelectedIdx(parseInt(e.target.value));
						setPriceWithFee(undefined);
						return;
					}}
				>
					<option value={ '' }>Choose payment token</option>
					{
						subscriptionsFound?.payWith.map((item) => {
							return (
								<option value={ item.paymentToken }>{ getPaymentToken(item.paymentToken)?.symbol || compactString(item.paymentToken) }</option>
							)
						})
					}
				</select>
			</div>
		)
	}
	const isSubmitDisabled = () => {
		if ( subscriptionTariffSelectedIdx === -1 ) { return true; }
		if ( subscriptionTokenSelected === '' ) { return true; }
		if ( recieverEdited ) {
			if ( buyFor === '' ) { return true; }
			return false;
		}
		// if ( !this.subscriptionDispatcher.isLocked() && !!subscriptionRemainings && buyFor === '' ) { return true; }

		return false
	}

	const isMeCheckboxChecked = () => {
		// if ( !this.subscriptionDispatcher.isLocked() && !!subscriptionRemainings ) { return false; }

		return !recieverEdited;
	}

	return (
		<div className="modal">
		<div
				className="modal__inner"
				onMouseDown={(e) => {
					e.stopPropagation();
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner' || (e.target as HTMLTextAreaElement).className === 'container') {
						closePopup();
					}
				}}
			>
		<div className="modal__bg"></div>
		<div className="container">
		<div className="modal__content">
			<div
				className="modal__close"
				onClick={() => {
					closePopup();
				}}
			>
				<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
					<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
				</svg>
			</div>
			<div className="c-add">
				<div className="c-add__text">
					<div className="h2">SAFT Subscription</div>
				</div>

				<div className="c-add__form">

					<div className="row mb-3">
						<div className="col-12"><span className="input-label">Subscription receiver</span></div>
						<div className="col-12">
							<div className="row mb-2">
								<div className="col-auto my-1">
									<label className="checkbox">
										<input
											type="checkbox"
											name="receiver"
											checked={ isMeCheckboxChecked() }
											// disabled={ !this.subscriptionDispatcher.isLocked() && !!subscriptionRemainings }
											onChange={() => { setRecieverEdited(false) }}
										/>
											<span className="check"> </span>
											<span className="check-text">Me</span>
									</label>
								</div>
								<div className="col-auto my-1">
									<label className="checkbox">
										<input
											type="checkbox"
											name="receiver"
											checked={ recieverEdited }
											onChange={() => { setRecieverEdited(true) }}
										/>
											<span className="check"> </span>
											<span className="check-text">Another person</span>
									</label>
								</div>
							</div>
						</div>
						{
							recieverEdited ? (
								<div className="col-12">
									<textarea
										className="input-control"
										placeholder="Paste token address"
										rows={1}
										value={ buyFor }
										onChange={(e) => { setBuyFor( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
									></textarea>
								</div>
							) : null
						}
					</div>

					<div className="row">

						<div className="col-sm-7 mb-5 mb-sm-0">
							{ getPaymentTypeSelect() }
							{ getSubscriptionTypeSelect() }
							{ getSubscriptionPlanSelect() }
							{ getSubscriptionTokenSelect() }
						</div>

						<div className="col-sm-5">
							<div className="c-wrap mb-0">
								{ getTicketDetails() }
								<Web3Dispatcher.Consumer>
									{ web3dispatcher => (
										<button
											className="btn btn-grad w-100"
											disabled={ isSubmitDisabled() }
											onClick={() => {
												if ( subscriptionTariffSelectedIdx === -1 ) { return; }
												const receiver = buyFor !== '' ? buyFor : userAccount?.userAddress || '';

												const tariffSelected = subscriptionTariffs.find((item) => { return item.idx === subscriptionTariffSelectedIdx });
												if ( !tariffSelected ) { return; }
												const payOptionSelected = tariffSelected.payWith.find((item) => { return item.paymentToken.toLowerCase() === subscriptionTokenSelected.toLowerCase() })
												if ( !payOptionSelected ) { return; }

												const web3 = web3dispatcher.getForce();

												buySubscription(
													web3,
													registryContract,
													serviceContract,
													subscriptionTariffSelectedIdx,
													payOptionSelected,
													receiver
												)
													.then(() => {
														setTimeout(() => {
															// this.subscriptionDispatcher.checkSubcription();
															closePopup();
														}, 1000)
													});
											}}
										>Subscribe</button>
									)}
								</Web3Dispatcher.Consumer>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		</div>
		</div>
		</div>
	)
}