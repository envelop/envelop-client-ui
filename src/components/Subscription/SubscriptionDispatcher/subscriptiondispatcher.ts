
import { BigNumber, SubscriptionRemainings, SubscriptionTariff } from "envelop-client-core";
import React from "react";
import { useAppSelector } from "../../../redux/store";

export type SubscriptionDispatcherType = {
	isLocked: (address: string) => boolean,
	checkSubcription: (address: string) => SubscriptionRemainings,
	fetchTariffs: (address: string) => Array<SubscriptionTariff>,
}

// const currentChain = useAppSelector((state) => { return state.currentChain });

// const fetchTariffs = () => {
// 	if ( !currentChain ) { return; }

// 	getTariffsForService(
// 		currentChain.chainId,
// 		registryContract,
// 		agentContract,
// 		serviceContract,
// 	)
// 		.then((data: Array<SubscriptionTariff>) => {
// 			if ( !data ) { return; }

// 			data.forEach((item) => {
// 				item.payWith.forEach((iitem) => {
// 					getERC20Params(currentChain.chainId, iitem.paymentToken, userAccount?.userAddress, registryContract)
// 						.then((ddata) => {
// 							setPaymentTokens([
// 								...paymentTokens
// 									.filter((iiitem) => { return iiitem.contractAddress.toLowerCase() !== ddata.contractAddress.toLowerCase() }),
// 								ddata
// 							])
// 						})
// 				})
// 			})

// 			setSubscriptionTariffs(data);
// 		})
// }
// const fetchTicket = () => {
// 	if ( !currentChain ) { return; }

// 	getUserTicketForService(
// 		currentChain.chainId,
// 		registryContract,
// 		agentContract,
// 		serviceContract,
// 	)
// 		.then((data: SubscriptionRemainings | undefined) => {
// 			if ( !data ) { return; }

// 			setSubscriptionRemainings(data)
// 		})
// }

// const subscriptionRemainings = await getUserTicketForService(
// 	currentChain.chainId,
// 	registryContract,
// 	agentContract,
// 	serviceContract,
// );
// const subscriptionTariffs;

export const SubscriptionDispatcher = React.createContext<SubscriptionDispatcherType>({
	isLocked: (address: string) => { return true; },
	checkSubcription: (address: string) => { return { timeRemaining: new BigNumber(0), txRemaining: new BigNumber(0) }; },
	fetchTariffs: (address: string) => { return []; },
});