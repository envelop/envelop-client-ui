import { SubscriptionDispatcher, SubscriptionDispatcherType } from './subscriptiondispatcher';

export default SubscriptionDispatcher;

export type { SubscriptionDispatcherType }