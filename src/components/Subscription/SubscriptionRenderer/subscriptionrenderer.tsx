
import React, { useState } from 'react';
import {
	useAppDispatch,
	useAppSelector
} from '../../../redux/store';
import SubscriptionPopup from '../SubscriptionPopup';
import {
	BigNumber,
	SubscriptionRemainings,
} from 'envelop-client-core';
import { timeToDays } from '../../../_utils';

type SubscriptionRendererProps = {
	// subscriptionDispatcher: SubscriptionDispatcher,
	serviceItemName       : { singular: string, plural: string },
}

export default function SubscriptionRenderer(props: SubscriptionRendererProps) {

	// subscriptionDispatcher : SubscriptionDispatcher;

	const erc20List    = useAppSelector((state) => { return state.erc20List    });
	const currentChain = useAppSelector((state) => { return state.currentChain });
	const userAccount  = useAppSelector((state) => { return state.account      });

	// const { token, closePopup } = props;

	const dispatch = useAppDispatch();

	const [ subscriptionRemainings,          setSubscriptionRemainings          ] = useState<SubscriptionRemainings | undefined>(undefined);
	const [ subscriptionPopupOpened,         setSubscriptionPopupOpened         ] = useState(false);

	const getSubscriptionBlock = () => {

		if ( !subscriptionRemainings ) { return null; }

		// if ( this.subscriptionDispatcher.isLocked() ) {
		// 	return (
		// 		<div className="bw-subscib">
		// 			<div className="d-inline-block mr-2 my-3">To use the Batch Wrap you need to  </div>
		// 			<button
		// 				className="btn btn-outline"
		// 				onClick={() => {
		// 					this.setState({
		// 						subscriptionPopupOpened      : true,
		// 						subscriptionTariffs          : this.subscriptionDispatcher.availableTariffs,
		// 						paymentTokens                : this.subscriptionDispatcher.paymentTokens,
		// 						subscriptionTariffSelectedIdx: -1,
		// 						subscriptionTokenSelected     : '',
		// 					});
		// 				}}
		// 			>Subscribe</button>
		// 		</div>
		// 	)
		// }

		if ( !subscriptionRemainings ) {
			return null;
		}

		if ( parseInt(`${subscriptionRemainings.txRemaining}`) !== 0 ) {
			return (
				<div className="bw-subscib">
					You have <span className="days">{ subscriptionRemainings.txRemaining.toString() } { subscriptionRemainings.txRemaining.eq(1) ? 'wrap' : 'wraps' }</span> left
					<button
						className="btn btn-outline ml-3"
						onClick={() => {
							setSubscriptionPopupOpened(true);
						}}
					>+ Subscription</button>
				</div>
			)
		}

		const now = new BigNumber(new Date().getTime()).dividedBy(1000);
		const diff = subscriptionRemainings.timeRemaining.minus(now);

		return (
			<div className="bw-subscib">
				Your subscription expires in <span className="days">{ timeToDays(diff) }</span>
				<button
					className="btn btn-outline ml-3"
					onClick={() => {
						setSubscriptionPopupOpened(true);
					}}
				>+ Subscription</button>
			</div>
		)
	}

	return (
		<React.Fragment>
		{ getSubscriptionBlock() }
		{
			subscriptionPopupOpened ? (
				<SubscriptionPopup
					registryContract = { '' }
					agentContract = { '' }
					serviceContract = { '' }
					TX_names = { { singular: 'wrap', plural: 'wraps' } }
					closePopup = { () => { setSubscriptionPopupOpened(false) } }
				/>
			) : null
		}
		</React.Fragment>
	)
}