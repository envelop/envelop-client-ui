
import { useAppSelector } from "../../redux/store";
import AdvancedLoader from "../AdvancedLoader";

export default function AdvancedLoaderRenderer() {

	const loader = useAppSelector((state) => { return state._advancedLoading });

	if ( !loader ) { return null; }

	return <AdvancedLoader { ...loader } />
}