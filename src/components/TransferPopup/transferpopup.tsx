
import React, {
	useState
} from "react";
import {
	Web3,
	BigNumber,
	NFTorWNFT,
	_AssetType,
	getERC20Params,
	getWNFTWrapperContract,
	getWNFTv0TransferModel,
	isAddress,
	isContractWNFTFromChain,
	makeERC20Allowance,
	transferERC1155,
	transferERC721,
} from "envelop-client-core";
import {
	useAppDispatch,
	useAppSelector
} from "../../redux/store";
import {
	AdvancedLoadingStatus,
	ModalTypes
} from "../../_types";
import {
	setInfoModal,
	setLoading,
	unsetLoading
} from "../../redux/basicActions";
import Web3Dispatcher from "../Web3Dispatcher";
import {
	createAdvancedLoading,
	updateStepAdvancedLoading
} from "../../redux/basicActions";

type TransferPopupProps = {
	token     : NFTorWNFT,
	closePopup: Function,
}

export default function TransferPopup(props: TransferPopupProps) {

	const { token, closePopup } = props;

	const currentChain = useAppSelector((state) => { return state.currentChain });
	const userAccount  = useAppSelector((state) => { return state.account      });

	const dispatch = useAppDispatch();

	const [ transferAddress, setTransferAddress ] = useState('');
	const [ amount,          setAmount          ] = useState('');

	const v0AppLink = 'https://app.envelop.is';

	const transferSuccess = (data: any) => {
		dispatch(unsetLoading());
		dispatch(setInfoModal({
			type: ModalTypes.success,
			data: {
				text: `Our token has been transferred (${transferAddress})`,
				links: [{
					text: `View on ${currentChain?.explorerName}`,
					url: `${currentChain?.explorerBaseUrl}/tx/${data.transactionHash}`
				}]
			}
		}));
		closePopup();
	}
	const transferError = (e: any) => {
		dispatch(unsetLoading());
		dispatch(setInfoModal({
			type: ModalTypes.error,
			data: {
				text: `Cannot transfer token: ${e}`
			}
		}));
		closePopup();
	}

	const transferNFT = async (web3: Web3) => {

		if ( !userAccount ) { return; }

		dispatch(setLoading('Waiting for transfer'));

		const { contractAddress, tokenId } = token;
		if ( props.token.assetType === _AssetType.ERC1155 ) {
			const amountToSend = amount !== '' && !(new BigNumber(amount).isNaN()) ? new BigNumber(amount) : new BigNumber('1');
			await transferERC1155(web3, contractAddress, tokenId, amountToSend, userAccount.userAddress, transferAddress)
				.then((data) => { transferSuccess(data) })
				.catch((e) => { transferError(e) });
		} else {
			await transferERC721(web3, contractAddress, tokenId, userAccount.userAddress, transferAddress)
				.then((data) => { transferSuccess(data) })
				.catch((e) => { transferError(e) });
		}
	}

	const transferWNFT = async (web3: Web3) => {

		if ( !userAccount ) { return; }

		dispatch(createAdvancedLoading({
			title: 'Transferring WNFT',
			stages: [
				{
					id: 'approveerc20',
					sortOrder: 1,
					text: 'Checking fee',
					status: AdvancedLoadingStatus.loading
				},
				{
					id: 'transferwnft',
					sortOrder: 10,
					text: 'Transferring WNFTv0',
					status: AdvancedLoadingStatus.queued
				},
			]
		}));

		const { contractAddress, tokenId, fees } = token;

		if ( !fees || !fees.length ) {
			transferNFT(web3);
			return;
		}

		const wrapperContract = await getWNFTWrapperContract(currentChain?.chainId || 1, contractAddress);
		const feeToken        = await getERC20Params(currentChain?.chainId || 1, fees[0].token, userAccount.userAddress, wrapperContract);
		const feeAmount       = new BigNumber(fees[0].value);

		if ( feeToken.balance.lt(feeAmount) ) {
			dispatch(unsetLoading());
			dispatch(setInfoModal({
				type: ModalTypes.error,
				data: {
					text: `Not enough ${feeToken.symbol}`
				}
			}));
		}

		const allowance = feeToken.allowance.find((item) => { return item.allowanceTo === wrapperContract; })
		if ( allowance && allowance.amount.lt(feeAmount) ) {
			try {
				await makeERC20Allowance(
					web3,
					feeToken.contractAddress,
					userAccount.userAddress,
					feeAmount,
					wrapperContract
				);
			} catch(e: any) {
				dispatch(unsetLoading());
				dispatch(setInfoModal({
					type: ModalTypes.error,
					data: {
						text: `Cannot approve ${feeToken.symbol}: ${e}`
					}
				}));
			}
		}

		dispatch(updateStepAdvancedLoading([{
				id: 'approveerc20',
				status: AdvancedLoadingStatus.complete
			},
			{
				id: 'transferwnft',
				status: AdvancedLoadingStatus.loading
			}
		]));

		if ( props.token.assetType === _AssetType.ERC1155 ) {
			const amountToSend = amount !== '' && !(new BigNumber(amount).isNaN()) ? new BigNumber(amount) : new BigNumber('1');
			await transferERC1155(web3, contractAddress, tokenId, amountToSend, userAccount.userAddress, transferAddress)
				.then((data) => { transferSuccess(data) })
				.catch((e) => { transferError(e) });
		} else {
			await transferERC721(web3, contractAddress, tokenId, userAccount.userAddress, transferAddress)
				.then((data) => { transferSuccess(data) })
				.catch((e) => { transferError(e) });
		}
	}

	const transferWNFTv0 = async (web3: Web3) => {

		if ( !currentChain ) { return; }
		if ( !userAccount  ) { return; }

		const { contractAddress, tokenId, fees } = token;

		dispatch(createAdvancedLoading({
			title: 'Transferring WNFTv0',
			stages: [
				{
					id: 'approveerc20',
					sortOrder: 1,
					text: 'Checking fee',
					status: AdvancedLoadingStatus.loading
				},
				{
					id: 'transferwnft',
					sortOrder: 10,
					text: 'Transferring WNFTv0',
					status: AdvancedLoadingStatus.queued
				},
			]
		}));

		if ( fees && fees.length ) {
			const allowanceAddress = await getWNFTv0TransferModel(
				currentChain?.chainId,
				contractAddress,
				fees[0].token,
			)

			const feeToken  = await getERC20Params(currentChain?.chainId || 1, fees[0].token, userAccount.userAddress, allowanceAddress);
			const feeAmount = new BigNumber(fees[0].value);

			const allowance = feeToken.allowance.find((item) => { return item.allowanceTo === allowanceAddress; })
			if ( allowance && allowance.amount.lt(feeAmount) ) {
				try {
					await makeERC20Allowance(
						web3,
						feeToken.contractAddress,
						userAccount.userAddress,
						feeAmount,
						allowanceAddress
					);
				} catch(e: any) {
					dispatch(unsetLoading());
					dispatch(setInfoModal({
						type: ModalTypes.error,
						data: {
							text: `Cannot approve ${feeToken.symbol}: ${e}`
						}
					}));
				}
			}
		} else {
			dispatch(updateStepAdvancedLoading([{
					id: 'approveerc20',
					status: AdvancedLoadingStatus.complete
				},
				{
					id: 'transferwnft',
					status: AdvancedLoadingStatus.loading
				}
			]));


			transferERC721(
				web3,
				contractAddress,
				tokenId,
				userAccount?.userAddress,
				transferAddress
			)
				.then((data: any) => {
					dispatch(setInfoModal({
						type: ModalTypes.success,
						data: {
							text: `${'Our token has been transferred'} (${transferAddress})`,
							links: [{
								text: `View on ${currentChain.explorerName}`,
								url: `${currentChain.explorerBaseUrl}/tx/${data.transactionHash}`
							}]
						}
					}));
					dispatch(unsetLoading());
					closePopup();
				})
				.catch((e) => {
					console.log('Transfer wnftv0 failed', e);
					dispatch(unsetLoading());
					dispatch(setInfoModal({
						type: ModalTypes.error,
						data: {
							text: `Cannot transfer WNFTv0: ${e.message}`,
						}
					}));
				})
		}
	}

	const isTransferBtnDisabled = (): boolean => {
		if ( transferAddress === '' ) { return true; }

		if ( !isAddress(transferAddress) ) { return true; }

		if ( props.token.assetType === _AssetType.ERC1155 ) {
			if ( amount === '' ) { return true; }
			if ( props.token.amount && props.token.amount.lt(new BigNumber(amount)) ) { return true; }
			return false;
		}

		return false;
	}
	const getTransferSubmitBtn = () => {

		return (
			<Web3Dispatcher.Consumer>
				{ web3dispatcher => (
					<button
						className="btn w-100"
						type="button"
						disabled={ isTransferBtnDisabled() }
						onClick={async (e) => {
							if ( isTransferBtnDisabled() ) { return; }
							if ( !transferAddress ) { return; }
							if ( !currentChain ) { return; }

							let web3;
							try {
								web3 = await web3dispatcher.getForce();
							} catch(e: any) {
								console.log('You shouldda connect wallet');
								return;
							}

							const isWNFT = await isContractWNFTFromChain(currentChain?.chainId, props.token.contractAddress, props.token.tokenId);
							if ( isWNFT === '' ) {
								transferNFT(web3);
								return;
							}

							if ( props.token.assetType === _AssetType.wNFTv0 ) {
								transferWNFTv0(web3);
								return;
							}

							transferWNFT(web3);
						}}
					>
						{ 'Accept' }
					</button>
				)}
			</Web3Dispatcher.Consumer>
		)

	}
	const getV0Label = () => {
		if ( props.token.assetType !== _AssetType.wNFTv0 ) { return null; }
		if ( !currentChain ) { return null; }

		const tokenUrl = `${v0AppLink}/#/token?chain=${currentChain.chainId || 0}&contractAddress=${ props.token.contractAddress }&tokenId=${ props.token.tokenId }`

		return (
			<React.Fragment>
				<p className="text-orange">
					This NFT has been wrapped by the previous version of dApp. Some functions may not work properly with current version of dApp.
					<br />
					<a target="_blank" rel="noopener noreferrer" href={ tokenUrl } >Go to v0 app.</a>
				</p>
			</React.Fragment>
		)
	}

	return (
		<div className="modal">
			<div
				className="modal__inner"
				onClick={(e) => {
					e.stopPropagation();
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						closePopup();
					}
				}}
			>
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">
						<div
							className="modal__close"
							onClick={() => { closePopup() }}
						>
							<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
								<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
							</svg>
						</div>
						<div className="c-add">
							<div className="c-add__text">
								<div className="h2">{ 'Transfer token' }</div>
								<p>{ 'After this action you will not own this wrapped NFT' }</p>
								{ getV0Label() }
							</div>
							<div className="c-add__coins">
								<div className="c-add__form">

										<div className="form-row">
											<div className="col col-12">
												<div className="input-group">
													<textarea
														className="input-control"
														placeholder={ 'Paste address' }
														value={ transferAddress }
														onChange={(e) => { setTransferAddress(e.target.value) }}
														rows={1}
													></textarea>
												</div>
											</div>
											{
												props.token.assetType === _AssetType.ERC1155 ? (
													<div className="col col-12 col-sm-4">
														<div className="input-group">
															<input
																className="input-control"
																type="text"
																placeholder="Amount"
																value={ amount }
																onChange={(e) => {
																	const value = e.target.value.replaceAll(' ', '');
																	if (
																		value === '' ||
																		isNaN(parseInt(value))
																	) {
																		setAmount('');
																		return;
																	}

																	setAmount(`${parseInt(value)}`);
																}}
															/>
														</div>
													</div>
												) : null
											}
											<div className="col col-12 col-sm-4">
												{ getTransferSubmitBtn() }
											</div>
										</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}