
import React                from 'react';
import Web3Dispatcher       from '../Web3Dispatcher';
import Footer               from '../Footer';
import Header               from '../Header';

import { Provider }         from 'react-redux';
import { BrowserRouter }    from 'react-router-dom';
import { InfoModalProps }   from '../../_types';
import {
	setCurrentChain,
	updateAccount
}   from '../../redux/basicActions';
import {
	store,
	StoreType,
} from '../../redux/store';

import {
	erc20TokenUpdate,
} from '../../redux/basicActions';
import {
	_AssetType,
	ERC20Type,
	getChainParamsFromAPI,
	getERC20Params,
	getNFTById,
	getWNFTById,
	NFT,
	Web3,
	WNFT,
	connect,
	connectSilent,
	getChainId,
	getNativeBalance,
	getUserAddress,
	isContractWNFTFromChain,
	createContract,
	getDefaultWeb3,
} from 'envelop-client-core';

import NFTCard, {
	TokenRenderType
} from '../NFTCard';

type AppParamsType = {
}
type AppState = {
	infoModal: InfoModalProps | undefined,
	erc20: Array<ERC20Type>,
	nft?: NFT,
	wnft721?: WNFT,
	wnft1155?: WNFT,
	wnftv0?: WNFT,
}

class App extends React.Component<AppParamsType, AppState> {

	store: StoreType;
	unsubscribe!   : Function;
	web3: undefined | Web3;

	constructor(props: AppParamsType) {
		super(props);

		this.store = store;

		this.state = {
			infoModal: undefined,
			erc20: [],
		};
	}

	componentDidMount(): void {

		const dispatch = this.store.dispatch;

		connectSilent().then(async (data) => {
			if ( !data ) { return; }
			this.web3 = data;

			const userAddress = await getUserAddress(this.web3);
			if ( !userAddress ) { return; }
			const chainId = await getChainId(this.web3);
			if ( !chainId ) { return; }
			const balanceNative = await getNativeBalance(chainId, userAddress);
			dispatch(updateAccount({ userAddress, balanceNative }));

			getChainParamsFromAPI(chainId).then((data) => { dispatch(setCurrentChain( data )) })
		});

		this.unsubscribe = this.store.subscribe(() => {
			this.setState({
				erc20: this.store.getState().erc20List,
			});
		});

		// isContractWNFTFromChain(5, '0x4b44874f117e04462bf005779e5f2d021f1688b8', '51')
		// 	.then((data) => {
		// 		console.log('nft', data);
		// 	})
		// isContractWNFTFromChain(5, '0x3b47910b3aabed960b1773dfe1de299d51655720', '56')
		// 	.then((data) => {
		// 		console.log('wnft 721', data);
		// 	})
		// isContractWNFTFromChain(5, '0x12892c9c15addb41656a23f6ed758f8cbcc210ac', '63')
		// 	.then((data) => {
		// 		console.log('wnftv0', data);
		// 	})
		// isContractWNFTFromChain(5, '0xf69f67CEa3cfF70812B2e914181572838c194ca0', '10')
		// 	.then((data) => {
		// 		console.log('wnft 1155', data);
		// 	})


		// getERC20Params(5, '0x376e8EA664c2E770E1C45ED423F62495cB63392D')
		// 	.then((data) => {
		// 		console.log('erc20', data);
		// 		if ( data ) { this.store.dispatch( erc20TokenUpdate(data)); }
		// 	})

		getNFTById(5, '0x4b44874f117e04462bf005779e5f2d021f1688b8', '51')
			.then((data) => {
				this.setState({ nft: data })
			})
		getWNFTById(5, '0x12892c9c15addb41656a23f6ed758f8cbcc210ac', '63')
			.then((data) => {
				this.setState({ wnftv0: data })
			})
		getWNFTById(5, '0x3b47910b3aabed960b1773dfe1de299d51655720', '56')
			.then((data) => {
				// console.log('wnft', data);
				this.setState({ wnft721: data })
			})
		getWNFTById(5, '0xf69f67CEa3cfF70812B2e914181572838c194ca0', '10')
			.then((data) => {
				// console.log('wnft 1155', data);
				this.setState({ wnft1155: data })
			})
	}
	componentWillUnmount() { this.unsubscribe(); }

	async getWeb3Data() {
		if ( !this.web3 ) { return; }
		if ( !this.store.getState().account ) {

			const userAddress = await getUserAddress(this.web3);
			if ( !userAddress ) { return; }
			const chainId = await getChainId(this.web3);
			if ( !chainId ) { return; }
			const balanceNative = await getNativeBalance(chainId, userAddress);

			const dispatch = this.store.dispatch;
			dispatch(updateAccount({ userAddress, balanceNative }));
		}
	}
	getOrConnectWeb3 = async () => {
		if ( this.web3 ) {
			if ( !this.store.getState().account ) {
				if ( this.web3 ) {
					const userAddress = await getUserAddress(this.web3);
					if ( !userAddress ) { return; }
					const chainId = await getChainId(this.web3);
					if ( !chainId ) { return; }
					const balanceNative = await getNativeBalance(chainId, userAddress);

					const dispatch = this.store.dispatch;
					dispatch(updateAccount({ userAddress, balanceNative }));
				}
			}
			return this.web3;
		}
		this.web3 = await connect();
		if ( !this.store.getState().account ) {
			if ( this.web3 ) {
				const userAddress = await getUserAddress(this.web3);
				if ( !userAddress ) { return; }
				const chainId = await getChainId(this.web3);
				if ( !chainId ) { return; }
				const balanceNative = await getNativeBalance(chainId, userAddress);

				const dispatch = this.store.dispatch;
				dispatch(updateAccount({ userAddress, balanceNative }));
			}
		}

		return this.web3;
	}
	getWeb3 = async () => {
		if ( this.web3 ) {
			if ( !this.store.getState().account ) {
				if ( this.web3 ) {
					const userAddress = await getUserAddress(this.web3);
					if ( !userAddress ) { return; }
					const chainId = await getChainId(this.web3);
					if ( !chainId ) { return; }
					const balanceNative = await getNativeBalance(chainId, userAddress);

					const dispatch = this.store.dispatch;
					dispatch(updateAccount({ userAddress, balanceNative }));
				}
			}
			return this.web3;
		}
		this.web3 = await connectSilent();
		if ( !this.web3 ) { return; }
		if ( !this.store.getState().account ) {
			if ( this.web3 ) {
				const userAddress = await getUserAddress(this.web3);
				if ( !userAddress ) { return; }
				const chainId = await getChainId(this.web3);
				if ( !chainId ) { return; }
				const balanceNative = await getNativeBalance(chainId, userAddress);

				const dispatch = this.store.dispatch;
				dispatch(updateAccount({ userAddress, balanceNative }));
			}
		}
		return this.web3;
	}

	render() {

		return (
			<BrowserRouter>
			<Provider store={store}>
			<Web3Dispatcher.Provider value={{ get: this.getWeb3, getForce: this.getOrConnectWeb3 }}>

				<Header />

					<h1>This is demo app with UI components</h1>

					<div className="db-section">
					<div className="container">
					<div className="c-row">
					<div className="c-col">
						{
							this.state.nft ? (
								<NFTCard
									token={ this.state.nft }
									tokenType={ TokenRenderType.discovered }
								/>
							) : null
						}
					</div>
					<div className="c-col">
						{
							this.state.wnft721 ? (
								<NFTCard
									token={ this.state.wnft721 }
									tokenType={ TokenRenderType.wrapped }
								/>
							) : null
						}
					</div>
					<div className="c-col">
						{
							this.state.wnft1155 ? (
								<NFTCard
									token={ this.state.wnft1155 }
									tokenType={ TokenRenderType.wrapped }
								/>
							) : null
						}
					</div>
					<div className="c-col">
						{
							this.state.wnftv0 ? (
								<NFTCard
									token={ this.state.wnftv0 }
									tokenType={ TokenRenderType.wnftv0 }
								/>
							) : null
						}
					</div>
					</div>
					</div>
					</div>

				<Footer />
			</Web3Dispatcher.Provider>
			</Provider>
			</BrowserRouter>
		)
	}
}

export default App;