
import InfoModal from '../InfoModal/infomodal';
import {
	useAppSelector
} from '../../redux/store';

export default function InfoModalRenderer() {

	const modal = useAppSelector((state) => { return state._info });

	if ( !modal ) { return null; }

	return (
		<InfoModal { ...modal } />
	)
}