import React, {
	useState
} from 'react';
import Tippy from '@tippyjs/react';
import {
	BigNumber,
	ERC20Type,
	WNFT,
	getMaxCollateralSlots,
	getWNFTWrapperContract,
	tokenToInt,
	Lock,
	LockType,
	CollateralItem,
	_AssetType,
	tokenToFloat,
	addThousandSeparator,
	removeThousandSeparator,
	Web3,
	getERC20BalanceFromChain,
	ERC20Balance,
	getERC20Params,
	getNFTById,
} from 'envelop-client-core';

import icon_attention        from '../../static/pics/icons/i-attention.svg';
import icon_external         from '../../static/pics/icons/i-external.svg';
import default_icon          from 'envelop-client-core/static/pics/coins/_default.svg';

import {
	useAppDispatch,
	useAppSelector
} from '../../redux/store';
import CoinSelector from '../CoinSelector';
import CollateralViewer from '../CollateralViewer';
import {
	erc20TokenUpdate,
	erc20TokenUpdateBalance
} from '../../redux/basicActions';

type AddValuePopupProps = {
	token          : WNFT,
	closePopup     : Function,
}

type CollateralErrorMsgType = {
	address: string
	tokenId: string
	msg: string
}

export default function AddValuePopup(props: AddValuePopupProps) {

	const { token, closePopup } = props;

	const erc20List    = useAppSelector((state) => { return state.erc20List    });
	const currentChain = useAppSelector((state) => { return state.currentChain });
	const userAccount  = useAppSelector((state) => { return state.account      });

	const dispatch = useAppDispatch();

	const [ erc20Token,      setErc20Token                    ] = useState<ERC20Type | undefined>(undefined);
	const [ collaterals,     setCollaterals                   ] = useState<Array<CollateralItem>>([]);

	const [ inputAssetType,         setInputAssetType         ] = useState(_AssetType.native);
	const [ inputCollateralAddress, setInputCollateralAddress ] = useState('');
	const [ inputCollateralTokenId, setInputCollateralTokenId ] = useState('');
	const [ inputCollateralAmount,  setInputCollateralAmount  ] = useState('');
	const [ collateralErrors,       setCollateralErrors       ] = useState<Array<CollateralErrorMsgType>>([]);
	const [ erc20Balance,           setErc20Balance           ] = useState<ERC20Balance | undefined>(undefined);
	const [ maxSlots,               setMaxSlots               ] = useState<number | undefined>(undefined);
	const [ wrapperContract,        setWrapperContract        ] = useState<string | undefined>(undefined);

	const niftsyTokenIcon = 'https://envelop.is/assets/img/niftsy.svg';

	const getMaxSlots = async () => {
		const foundLock = token.locks.find((item: Lock) => { return item.lockType === LockType.slots });
		if ( foundLock ) { return foundLock.param.toNumber() }

		const wrapperAddress = await getWNFTWrapperContract(currentChain?.chainId || 0, token.contractAddress);
		if ( wrapperAddress ) {
			setWrapperContract(wrapperAddress);
		} else {
			setWrapperContract(token.contractAddress);
		}
		return await getMaxCollateralSlots(currentChain?.chainId || 0, wrapperAddress)
	}
	getMaxSlots().then((data) => { setMaxSlots(data) });

	const getAddValueTokenSelector = () => {
		return (
			<CoinSelector
				tokens        = { erc20List }
				selectedToken = { erc20Token ? erc20Token.contractAddress : '0x0000000000000000000000000000000000000000' }
				onChange      = {(address: string) => {
					if ( !address || address === '0x0000000000000000000000000000000000000000' ) {
						setErc20Token(undefined);
						return;
					}

					const tokenToAdd = erc20List.find((item) => {
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() === address.toLowerCase();
					});
					if ( tokenToAdd ) {
						setErc20Token(tokenToAdd);
						return;
					}

					setErc20Token(undefined);
				}}
			/>
		)
	}
	const getERC20Link = () => {
		if ( inputCollateralAddress === '' ) { return null; }

		let foundToken: ERC20Type | undefined = undefined;
		foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( !foundToken ) { return null; }

		let tokenImage = '';
		if ( foundToken.name.toLowerCase() === 'niftsy' ) {
			tokenImage = niftsyTokenIcon
		}

		return (
			<div className="d-flex align-items-center flex-wrap">
				<a
					className="ex-link mr-3 mt-3 mb-3"
					target="_blank"
					rel="noopener noreferrer"
					href={ `${currentChain?.explorerBaseUrl || ''}/token/${foundToken.contractAddress}` }
				>
					<small>More about { foundToken.symbol }</small>
					<img className="i-ex" src={ icon_external } alt="" />
				</a>
				<a
					className="btn btn-sm btn-gray"
					href="/"
					onClick={(e) => {
						e.preventDefault();
						if ( !foundToken ) { return; }
						try {
							if ( !(window as any).ethereum ) { return; }

							(window as any).ethereum.request({
								method: 'wallet_watchAsset',
								params: {
									type: 'ERC20', // Initially only supports ERC20, but eventually more!
									options: {
										address: foundToken.contractAddress, // The address that the token is at.
										symbol: foundToken.symbol, // A ticker symbol or shorthand, up to 5 chars.
										decimals: foundToken.decimals, // The number of decimals in the token
										image: tokenImage, // A string url of the token logo
									},
								},
							});
						} catch(e) {
							console.log(e)
						}
					}}
				>
					Add to Metamask
				</a>
			</div>
		)
	}
	const getAddValueBtn = () => {

		return (
			<div className="d-flex justify-content-center pt-4">
				<button
					className="btn btn-lg w-100"
					disabled={
						!collaterals.length ||
						!!collateralErrors.length
					}
					onClick={() => {
						// if ( !addValueToken ) { return; }
						// metamaskAdapter.wrapperContract.addCollateral({
						// 	token: addValueToken,
						// 	collaterals: collaterals,
						// 	callback: () => { closePopup(); }
						// })
					}}
				>Confirm</button>
			</div>
		)
	}

	const isNativeBalanceEnough = () => {
		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }

		const userBalance = userAccount ? userAccount.balanceNative : new BigNumber(10**50);
		if ( tokenToInt(amountParsed, currentChain?.decimals || 18).lte(userBalance) ) { return true; }

		return false;
	}
	const isCollateralNativeBtnDisabled = () => {
		if ( inputCollateralAmount === '' ) { return true; }

		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }

		if ( !isNativeBalanceEnough() ) { return true; }

		if ( maxSlots !== undefined ) {
			if ( new BigNumber(maxSlots).gt(token.collateral.length + collaterals.length) ) { return false; }
		}

		const foundExistindCollateral = token.collateral.find((item: CollateralItem) => { return item.assetType === _AssetType.native });
		if ( foundExistindCollateral ) { return false; }

		const foundAddingCollateral = collaterals.find((item: CollateralItem) => { return item.assetType === _AssetType.native });
		if ( foundAddingCollateral ) { return false; }

		return true;
	}
	const addCollateralNativeRow = () => {
		let amountNative = new BigNumber(0);
		const nativeAdded = collaterals.find((item: CollateralItem) => { return item.assetType === _AssetType.native });
		if ( nativeAdded && nativeAdded.amount ) {
			amountNative = new BigNumber(nativeAdded.amount);
		}
		const collateralsUpdated = [
			...collaterals.filter((item) => { return item.assetType !== _AssetType.native }),
			{
				assetType: _AssetType.native,
				contractAddress: '0x0000000000000000000000000000000000000000',
				amount: tokenToInt(new BigNumber(inputCollateralAmount), currentChain?.decimals || 18).plus(amountNative),
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
	}
	const getAddCollateralNativeBtn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralNativeBtnDisabled() }
				onClick={() => { addCollateralNativeRow() }}
			>Add</button>
		)
	}
	const getCollateralNativeBalance = () => {

		const balance = userAccount?.balanceNative || new BigNumber(0);
		const decimals = currentChain?.decimals || 18;

		return (
			<div className="c-add__max mt-3">
				<div>
					<span>Max: </span>
					<button
						onClick={() => {
							setInputCollateralAmount(tokenToFloat(balance, decimals).toString())
						}}
					>{ tokenToFloat(balance, decimals).toFixed(5) }</button>
				</div>
			</div>
		)
	}
	const getNativeAmountInput = () => {
		return (
			<React.Fragment>
				<div className="select-group">
					<input
						className={`input-control ${!isNativeBalanceEnough() ? 'has-error' : '' }`}
						type="text"
						placeholder="0.000"
						value={ addThousandSeparator(inputCollateralAmount) }
						onChange={(e) => {
							let value = removeThousandSeparator(e.target.value);
							if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
								if ( new BigNumber(value).isNaN() ) { return; }
								value = new BigNumber(value).toString();
							}
							setInputCollateralAmount(value);
						}}
						onKeyUp={(e) => {
							if ( e.defaultPrevented)                 { return; }
							if ( !!isCollateralNativeBtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                 { return; }

							addCollateralNativeRow()
						}}
					/>
					<div className="select-coin">
						<div className="select-coin__value">
							<span className="field-unit">
								<span className="i-coin">
									<img src={ currentChain?.tokenIcon || default_icon } alt="" />
								</span>
								{ currentChain?.symbol || '0x000' }
							</span>
						</div>
					</div>
				</div>
				{
					!isNativeBalanceEnough() ? (
						<div className="input-error">Not enough balance</div>
					) : null
				}
			</React.Fragment>
		)
	}
	const getCollateralNativeSlotsAlert = () => {
		if ( maxSlots !== undefined ) {
			if ( new BigNumber(maxSlots).gt(token.collateral.length + collaterals.length) ) { return false; }
		}

		const addressToFind = '0x0000000000000000000000000000000000000000';

		const foundExistindCollateral = token.collateral.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === addressToFind });
		if ( foundExistindCollateral ) { return null; }

		const foundAddingCollateral = collaterals.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === addressToFind });
		if ( foundAddingCollateral ) { return null; }

		return ( <div className="alert alert-warning mb-3">You have reached the maximum number of collateral slots</div> );
	}
	const getCollateralNativeBlock = () => {
		if ( inputAssetType === _AssetType.native ) {
			return (
				<React.Fragment>
					{ getCollateralNativeSlotsAlert() }
					<div className="row">
						<div className="col col-12 col-md-7">
							<label className="input-label">Amount</label>
							{ getNativeAmountInput() }
							{ getCollateralNativeBalance() }
						</div>
						<div className="col col-12 col-md-2">
							<label className="input-label">&nbsp;</label>
							{ getAddCollateralNativeBtn() }
						</div>
					</div>
				</React.Fragment>
			)
		}
	}

	const isERC20BalanceEnough = () => {
		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( erc20Balance?.balance.lt(tokenToInt(amountParsed, erc20Token?.decimals || 18)) ) { return false; }

		return true;
	}
	const isCollateralERC20BtnDisabled = () => {
		if ( inputCollateralAddress === '' ) { return true; }
		if ( inputCollateralAmount  === '' ) { return true; }
		if ( !isERC20BalanceEnough()       ) { return true; }

		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }

		if ( maxSlots !== undefined ) {
			if ( new BigNumber(maxSlots).gt(token.collateral.length + collaterals.length) ) { return false; }
		}

		const foundExistindCollateral = token.collateral.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( foundExistindCollateral ) { return false; }

		const foundAddingCollateral = collaterals.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( foundAddingCollateral ) { return false; }

		return true;
	}
	const addCollateralERC20Row = () => {
		let amountAdded = new BigNumber(0);
		let amountToAdd = new BigNumber(inputCollateralAmount);
		const erc20Added = collaterals.filter((item) => {
			if ( !item.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase()
		});
		if ( erc20Added.length && erc20Added[0].amount ) {
			amountAdded = new BigNumber(erc20Added[0].amount);
		}

		const foundToken = erc20List.find((item) => {
			if ( !item.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase()
		});
		if ( foundToken ) {
			amountAdded = tokenToFloat(new BigNumber(amountAdded), foundToken.decimals || 18);
			amountToAdd = tokenToInt(amountToAdd.plus(amountAdded), foundToken.decimals || 18);
		} else {
			amountToAdd = amountToAdd.plus(amountAdded)
		}

		const collateralsUpdated = [
			...collaterals.filter((item) => {
				if ( !item.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() !== inputCollateralAddress.toLowerCase()
			}),
			{
				assetType: _AssetType.ERC20,
				contractAddress: inputCollateralAddress,
				amount: amountToAdd,
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');

	}
	const getAddCollateralERC20Btn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralERC20BtnDisabled() }
				onClick={() => { addCollateralERC20Row() }}
			>Add</button>
		)
	}
	const getCollateralERC20AmountTitle = () => {
		const foundERC20 = erc20List.find((item) => {
			if ( !item.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase()
		});
		if (
			inputCollateralAddress &&
			inputCollateralAddress !== '' &&
			inputCollateralAddress !== '0' &&
			inputCollateralAddress !== '0x0000000000000000000000000000000000000000' &&
			!foundERC20
		) {
			return (
				<Tippy
					content={ 'decimals is unknown; enter amount in wei' }
					appendTo={ document.getElementsByClassName("wrapper")[0] }
					trigger='mouseenter'
					interactive={ false }
					arrow={ false }
					maxWidth={ 512 }
				>
					<label className="input-label text-orange">{ 'Amount' }*</label>
				</Tippy>
			)
		} else {
			return (
				<label className="input-label">
					{ 'Amount' }
					<Tippy
						content={ 'Maximum and allowanced amount of tokens which you can add to collateral of wrapped nft' }
						appendTo={ document.getElementsByClassName("wrapper")[0] }
						trigger='mouseenter'
						interactive={ false }
						arrow={ false }
						maxWidth={ 512 }
					>
						<span className="i-tip"></span>
					</Tippy>
				</label>
			)
		}
	}
	const updateERC20Balance = (address: string) => {
		if ( !Web3.utils.isAddress(address) ) { return; }

		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === address.toLowerCase() });

		if ( !foundToken ) {
			getERC20Params(currentChain?.chainId || 1, address, userAccount?.userAddress || '', wrapperContract)
				.then((data) => {
					dispatch(erc20TokenUpdate(data))
				})
			return;
		}

		if ( !foundToken.balance ) {
			getERC20BalanceFromChain(currentChain?.chainId || 1, address, userAccount?.userAddress || '', wrapperContract)
				.then((data) => {
					dispatch(erc20TokenUpdateBalance(data))
				})
			return;
		}

		const foundAllowance = foundToken.allowance.find((item) => { return item.allowanceTo.toLowerCase() === wrapperContract?.toLowerCase(); });
		if ( !foundAllowance ) {
			getERC20BalanceFromChain(currentChain?.chainId || 1, address, userAccount?.userAddress || '', wrapperContract)
				.then((data) => {
					dispatch(erc20TokenUpdateBalance(data))
				})
			return;
		}
	}
	const getCollateralERC20Balance = () => {
		if ( inputCollateralAddress === '' ) { return null; }

		const foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( !foundToken ) { return null; }

		console.log(foundToken);
		const foundAllowance = foundToken.allowance.find((item) => { return item.allowanceTo.toLowerCase() === wrapperContract?.toLowerCase(); });
		if ( !foundAllowance ) {
			return null;
		}

		return (
			<div className="c-add__max mt-2 mb-0">
				<div className="mt-1 mb-1">
					<span>Max: </span>
					<button
						onClick={() => { setInputCollateralAmount(tokenToFloat(foundToken.balance, foundToken.decimals || 18).toString()) }}
					>{ tokenToFloat(foundToken.balance, foundToken.decimals || 18).decimalPlaces(5).toString() }</button>
				</div>
				{
					erc20Balance && erc20Balance.allowance ? (
						<div>
							<span>Allowance: </span>
							<button
								onClick={() => {setInputCollateralAmount(tokenToFloat(foundAllowance.amount, foundToken.decimals || 18).toString()) }}
							>{ tokenToFloat(foundAllowance.amount, erc20Token?.decimals || 18).decimalPlaces(5).toString() }</button>
						</div>
					) : null
				}
			</div>
		)
	}
	const getCollateralERC20AddressInput = () => {
		return (
			<React.Fragment>
				<div className="select-group">
					<input
						className={`input-control ${ !isCollateralAddressInWhitelist() ? 'has-error' : '' }`}
						type="text"
						placeholder="0.000"
						value={ inputCollateralAddress }
						onChange={(e) => {
							const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
							if ( Web3.utils.isAddress(value) ) {
								setTimeout(() => { updateERC20Balance(value) }, 100);
							}

							setInputCollateralAddress(value);
							setErc20Balance(undefined);
						}}
						onKeyUp={(e) => {
							if ( e.defaultPrevented)                { return; }
							if ( !!isCollateralERC20BtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                { return; }

							addCollateralERC20Row()
						}}
					/>
					<CoinSelector
						tokens        = { erc20List }
						selectedToken = { inputCollateralAddress }
						onChange      = {(address: string) => {
							setTimeout(() => { updateERC20Balance(address) }, 100);
							setInputCollateralAddress(address);
							setErc20Balance(undefined);
						}}
					/>
				</div>
				{ !isCollateralAddressInWhitelist() ? ( <div className="input-error">Address is not in whitelist</div> ) : null }
			</React.Fragment>
		)
	}
	const getCollateralERC20AmountInput = () => {
		if ( !isERC20BalanceEnough() ) {
			return (
				<React.Fragment>
					<input
						className="input-control has-error"
						type="text"
						placeholder=""
						value={ addThousandSeparator(inputCollateralAmount) }
						onChange={(e) => {
							let value = removeThousandSeparator(e.target.value);
							if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
								if ( new BigNumber(value).isNaN() ) { return; }
								value = new BigNumber(value).toString();
							}
							setInputCollateralAmount(value);
						}}
						onKeyUp={(e) => {
							if ( e.defaultPrevented )               { return; }
							if ( !!isCollateralERC20BtnDisabled() ) { return; }
							if ( e.key !== 'Enter' )                { return; }

							addCollateralERC20Row()
						}}
					/>
					<div className="input-error">Not enough balance</div>
				</React.Fragment>
			)
		}

		return (
			<React.Fragment>
				<input
					className="input-control"
					type="text"
					placeholder=""
					value={ addThousandSeparator(inputCollateralAmount) }
					onChange={(e) => {
						let value = removeThousandSeparator(e.target.value);
						if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
							if ( new BigNumber(value).isNaN() ) { return; }
							value = new BigNumber(value).toString();
						}
						setInputCollateralAmount(value);
					}}
					onKeyUp={(e) => {
						if ( e.defaultPrevented )               { return; }
						if ( !!isCollateralERC20BtnDisabled() ) { return; }
						if ( e.key !== 'Enter' )                { return; }

						addCollateralERC20Row()
					}}
				/>
			</React.Fragment>
		)

	}
	const getCollateralERC20SlotsAlert = () => {
		if ( inputCollateralAddress === '' ) { return null; }

		if ( maxSlots !== undefined ) {
			if ( new BigNumber(maxSlots).gt(token.collateral.length + collaterals.length) ) { return false; }
		}

		const addressToFind = inputCollateralAddress.toLowerCase();

		const foundExistindCollateral = token.collateral.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === addressToFind });
		if ( foundExistindCollateral ) { return null; }

		const foundAddingCollateral = collaterals.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === addressToFind });
		if ( foundAddingCollateral ) { return null; }

		return ( <div className="alert alert-warning mb-3">You have reached the maximum number of collateral slots</div> );
	}
	const getCollateralERC20Block = () => {
		if ( inputAssetType === _AssetType.ERC20 ) {
			return (
				<React.Fragment>
					{ getCollateralERC20SlotsAlert() }
					<div className="row">
						<div className="col col-12 col-md-7">
							<label className="input-label">Token Address</label>
							{ getCollateralERC20AddressInput() }
							{ getERC20Link() }
						</div>
						<div className="col col-12 col-md-3">
							{ getCollateralERC20AmountTitle() }
							{ getCollateralERC20AmountInput() }
							{ getCollateralERC20Balance() }
						</div>
						<div className="col col-12 col-md-2">
							<label className="input-label">&nbsp;</label>
							{ getAddCollateralERC20Btn() }
						</div>
					</div>
				</React.Fragment>
			)
		}
	}

	const isCollateralERC721BtnDisabled = () => {
		const foundLock = token.locks.find((item: Lock) => { return item.lockType === LockType.slots });
		if ( foundLock ) {
			if ( foundLock.param.lt(collaterals.length + 1) ) { return true }
		}
		if ( maxSlots !== undefined ) {
			if ( collaterals.length > maxSlots - 1 ) { return true; }
		}
		if ( inputCollateralAddress === '' ) { return true; }
		if ( inputCollateralTokenId === '' ) { return true; }

		if ( !Web3.utils.isAddress(inputCollateralAddress) ) { return true; }
		// if ( collateralWhitelist ) {
		// 	const foundWhitelistItem = collateralWhitelist.find((item: _Asset) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		// 	if ( !foundWhitelistItem ) { return true; }
		// }

		return false;
	}
	const addCollateralERC721Row = () => {
		const address = inputCollateralAddress;
		const tokenId = inputCollateralTokenId;

		const collateralsUpdated = [
			...collaterals.filter((item) => {
					if ( item.assetType !== _AssetType.ERC721 ) { return true; }
					if ( !item.contractAddress ) { return false; }
					return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
						item.tokenId !== tokenId
				}),
			{
				assetType: _AssetType.ERC721,
				contractAddress: address,
				tokenId: tokenId,
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
		setInputCollateralTokenId('');

		getNFTById(currentChain?.chainId || 1, address, tokenId, userAccount?.userAddress)
			.then((data) => {

				let collateralErrorsUpdated = collateralErrors;
				if ( !data ) {
					setCollateralErrors([
						...collateralErrorsUpdated,
						{
							address: address,
							tokenId: tokenId,
							msg: 'Cannot find token'
						}
					]);
					return;
				}

				if ( data.owner && data.owner.toLowerCase() !== userAccount?.userAddress.toLowerCase() ) {
					collateralErrorsUpdated = [
						...collateralErrorsUpdated,
						{
							address: address,
							tokenId: tokenId,
							msg: 'Not yours'
						}
					]
				}

				setCollaterals([
					...collaterals.filter((item) => {
						if ( item.assetType !== _AssetType.ERC721 ) { return true; }
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
					}),
					{
						assetType: _AssetType.ERC721,
						contractAddress: address,
						tokenId: tokenId,
						tokenImg: data.image || ''
					}
				]);

			})
			.catch((e) => {
				console.log('Cannot get ERC721 collateral', e);
				let collateralErrorsUpdated = collateralErrors;
				setCollateralErrors([
					...collateralErrorsUpdated,
					{
						address: address,
						tokenId: tokenId,
						msg: e.message
					}
				]);
				return;
			})
	}
	const getAddCollateralERC721Btn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralERC721BtnDisabled() }
				onClick={() => { addCollateralERC721Row() }}
			>Add</button>
		)
	}
	const getCollateral721SlotsAlert = () => {
		if ( maxSlots === undefined ) { return null; }

		if ( new BigNumber(maxSlots).gt(token.collateral.length + collaterals.length) ) { return null; }

		return ( <div className="alert alert-warning mb-3">You have reached the maximum number of collateral slots</div> );
	}
	const getCollateralERC721Block = () => {
		if ( inputAssetType === _AssetType.ERC721 ) {
			return (
				<React.Fragment>
					{ getCollateral721SlotsAlert() }
					<div className="row">
						<div className="col col-12 col-md-7">
							<label className="input-label">NFT Address</label>
							<input
								className="input-control"
								type="text"
								placeholder="Paste here"
								value={ inputCollateralAddress }
									onChange={(e) => { setInputCollateralAddress(e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "")) }}
									onKeyUp={(e) => {
										if ( e.defaultPrevented)                { return; }
										if ( !!isCollateralERC20BtnDisabled() ) { return; }
										if ( e.key !== 'Enter' )                { return; }

										addCollateralERC721Row()
									}}
							/>
						</div>
						<div className="col col-12 col-md-3">
							<label className="input-label">Token ID</label>
							<input
								className="input-control"
								type="text"
								placeholder="99 900"
								value={ addThousandSeparator(inputCollateralTokenId) }
								onChange={(e) => { setInputCollateralTokenId(e.target.value.toLowerCase().replace(/[^0-9]/g, "")) }}
								onKeyUp={(e) => {
									if ( e.defaultPrevented)                { return; }
									if ( !!isCollateralERC20BtnDisabled() ) { return; }
									if ( e.key !== 'Enter' )                { return; }

									addCollateralERC721Row()
								}}
							/>
						</div>
						<div className="col col-12 col-md-2">
							<label className="input-label">&nbsp;</label>
							{ getAddCollateralERC721Btn() }
						</div>
					</div>
				</React.Fragment>
			)
		}
	}

	const isCollateralERC1155BtnDisabled = () => {
		const foundLock = token.locks.find((item: Lock) => { return item.lockType === LockType.slots });
		if ( foundLock ) {
			if ( foundLock.param.lt(collaterals.length + 1) ) { return true }
		}
		if ( maxSlots ) {
			if ( collaterals.length > maxSlots - 1 ) { return true; }
		}
		if ( inputCollateralAmount === '' ) { return true; }
		const amountParsed = new BigNumber(inputCollateralAmount);
		if ( !amountParsed || amountParsed.isNaN() || amountParsed.eq(0) ) { return true; }
		if ( inputCollateralAddress === '' ) { return true; }
		if ( inputCollateralTokenId === '' ) { return true; }

		// if ( collateralWhitelist ) {
		// 	const foundWhitelistItem = collateralWhitelist.find((item: _Asset) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		// 	if ( !foundWhitelistItem ) { return true }
		// }

		if ( !Web3.utils.isAddress(inputCollateralAddress) ) { return true; }

		return false;
	}
	const addCollateralERC1155Row = () => {
		const address = inputCollateralAddress;
		const tokenId = inputCollateralTokenId;
		const amount  = new BigNumber(inputCollateralAmount);

		const collateralsUpdated = [
			...collaterals.filter((item) => {
				if ( item.assetType !== _AssetType.ERC1155 ) { return true; }
				if ( !item.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
					item.tokenId !== tokenId
			}),
			{
				assetType: _AssetType.ERC1155,
				contractAddress: address,
				tokenId: tokenId,
				amount : amount,
			}
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
		setInputCollateralTokenId('');

		getNFTById(currentChain?.chainId || 1, address, tokenId, userAccount?.userAddress)
			.then((data) => {

				let collateralErrorsUpdated = collateralErrors;
				if ( !data ) {
					setCollateralErrors([
						...collateralErrorsUpdated,
						{
							address: address,
							tokenId: tokenId,
							msg: 'Cannot find token'
						}
					]);
					return;
				}

				if ( data.amount && data.amount.eq(0) ) {
						collateralErrorsUpdated = [
							...collateralErrorsUpdated,
							{
								address: address,
								tokenId: tokenId,
								msg: 'Not enough balance'
							}
						]
					}

				setCollaterals([
					...collaterals.filter((item) => {
						if ( item.assetType !== _AssetType.ERC1155 ) { return true; }
						if ( !item.contractAddress ) { return false; }
						return item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
					}),
					{
						assetType: _AssetType.ERC1155,
						contractAddress: address,
						tokenId: tokenId,
						tokenImg: data.image || ''
					}
				]);

			})
			.catch((e) => {
				console.log('Cannot get ERC1155 collateral', e);
				let collateralErrorsUpdated = collateralErrors;
				setCollateralErrors([
					...collateralErrorsUpdated,
					{
						address: address,
						tokenId: tokenId,
						msg: e.message
					}
				]);
				return;
			})
	}
	const getAddCollateralERC1155Btn = () => {
		return (
			<button
				className="btn btn-grad"
				disabled={ isCollateralERC1155BtnDisabled() }
				onClick={() => { addCollateralERC1155Row() }}
			>Add</button>
		)
	}
	const getCollateral1155SlotsAlert = () => {
		if ( inputCollateralAddress === '' ) { return null; }
		if ( inputCollateralTokenId === '' ) { return null; }

		if ( maxSlots === undefined ) { return null; }

		const addressToFind = inputCollateralAddress.toLowerCase();
		const tokenToFind   = inputCollateralTokenId;

		const foundExistindCollateral = token.collateral.find((item: CollateralItem) => {
			if ( !item.tokenId ) { return false; }
			return item.contractAddress.toLowerCase() === addressToFind && item.tokenId === tokenToFind
		});
		if ( foundExistindCollateral ) { return null; }

		const foundAddingCollateral = collaterals.find((item: CollateralItem) => {
			if ( !item.tokenId ) { return false; }
			return item.contractAddress.toLowerCase() === addressToFind && item.tokenId === tokenToFind
		});
		if ( foundAddingCollateral ) { return null; }

		return ( <div className="alert alert-warning mb-3">You have reached the maximum number of collateral slots</div> );
	}
	const getCollateralERC1155Block = () => {
		if ( inputAssetType === _AssetType.ERC1155 ) {
			return (
				<React.Fragment>
					{ getCollateral1155SlotsAlert() }
					<div className="row">
						<div className="col col-12 col-md-5">
							<label className="input-label">NFT Address</label>
							<input
								className="input-control"
								type="text"
								placeholder="Paste here"
								value={ inputCollateralAddress }
								onChange={(e) => { setInputCollateralAddress(e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ""))  }}
								onKeyUp={(e) => {
									if ( e.defaultPrevented )                    { return; }
									if ( !!isCollateralERC20BtnDisabled() ) { return; }
									if ( e.key !== 'Enter' )                     { return; }

									addCollateralERC1155Row()
								}}
							/>
						</div>
						<div className="col col-12 col-md-3">
							<label className="input-label">Token ID</label>
							<input
								className="input-control"
								type="text"
								placeholder="99 900"
								value={ addThousandSeparator(inputCollateralTokenId) }
								onChange={(e) => { setInputCollateralTokenId(e.target.value.toLowerCase().replace(/[^0-9]/g, "")) }}
								onKeyUp={(e) => {
									if ( e.defaultPrevented)                     { return; }
									if ( !!isCollateralERC20BtnDisabled() ) { return; }
									if ( e.key !== 'Enter' )                     { return; }

									addCollateralERC1155Row()
								}}
							/>
							</div>
						<div className="col col-12 col-md-2">
							<label className="input-label">Amount</label>
							<input
								className="input-control"
								type="text"
								placeholder="10"
								value={ addThousandSeparator(inputCollateralAmount) }
								onChange={(e) => {
									// let value = removeThousandSeparator(e.target.value);
									// if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
									// 	if ( new BigNumber(value).isNaN() ) { return; }
									// 	value = new BigNumber(value).toString();
									// }
									// setState({ inputCollateralAmount: value, })
									const value = removeThousandSeparator(e.target.value.replaceAll(' ', ''));
									if (
										value === '' ||
										isNaN(parseInt(value))
									) {
										setInputCollateralAmount('');
										return;
									}

									setInputCollateralAmount(`${parseInt(value)}`);
								}}
								onKeyUp={(e) => {
									if ( e.defaultPrevented)                     { return; }
									if ( !!isCollateralERC20BtnDisabled() ) { return; }
									if ( e.key !== 'Enter' )                     { return; }

									addCollateralERC20Row()
								}}
							/>
						</div>
						<div className="col col-12 col-md-2">
							<label className="input-label">&nbsp;</label>
							{ getAddCollateralERC1155Btn() }
						</div>
					</div>
				</React.Fragment>
			)
		}
	}

	const getCollateralItemAlert = () => {
		return collateralErrors.map((item) => { return ( <div className="alert alert-warning mb-3">{`Error with collateral: ${item.address}:${item.tokenId || ''}: ${item.msg}`}</div> ) })
	}
	const isCollateralAddressInWhitelist = () => {
		if ( inputCollateralAddress === '' ) { return true; }
		// if ( collateralWhitelist ) {
		// 	const foundWhitelistItem = collateralWhitelist.find((item: _Asset) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		// 	if ( !foundWhitelistItem ) { return false; }
		// }

		return true;
	}
	const getCollateralRows = () => {
		return (
			<CollateralViewer
				collaterals={ collaterals }
				collateralErrors={ collateralErrors }
				removeRow={(item: CollateralItem) => {
					const collateralsFiltered = collaterals.filter((iitem: CollateralItem) => {
						if ( item.assetType === _AssetType.ERC721 || item.assetType === _AssetType.ERC1155 ) {
							return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() ||
								item.tokenId !== iitem.tokenId
						} else {
							return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase()
						}
					});

					setCollaterals(collateralsFiltered);
					setCollateralErrors(collateralErrors.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.address.toLowerCase() || item.tokenId !== iitem.tokenId }));

				}}
				width={ 'wide' }
				color="light"
			/>
		)
	}
	const getCollateralBlock = () => {
		return (

				<div className="c-wrap__form">
					<p>You can add assets to collateral of your wrapped NFT. Use list of approved tokens.</p>
					<div className="row row-sm mb-5">
						<div className="col-12 col-sm-auto mr-3 py-2">
							<label className="input-label mb-0">Token Type</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.native }
									checked={ inputAssetType === _AssetType.native }
									onChange={() => {
										setInputAssetType(_AssetType.native);
										setInputCollateralAddress('');
										setInputCollateralAmount('');
										setInputCollateralTokenId('');
										setErc20Balance(undefined);
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">Native</span>
							</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.ERC20 }
									checked={ inputAssetType === _AssetType.ERC20 }
									onChange={() => {
										setInputAssetType(_AssetType.ERC20);
										setInputCollateralAddress('');
										setInputCollateralAmount('');
										setInputCollateralTokenId('');
										setErc20Balance(undefined);
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">{ currentChain?.EIPPrefix || 'ERC' }-20</span>
							</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.ERC721 }
									checked={ inputAssetType === _AssetType.ERC721 }
									onChange={() => {
										setInputAssetType(_AssetType.ERC721);
										setInputCollateralAddress('');
										setInputCollateralAmount('');
										setInputCollateralTokenId('');
										setErc20Balance(undefined);
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">{ currentChain?.EIPPrefix || 'ERC' }-721</span>
							</label>
						</div>
						<div className="col-auto py-2">
							<label className="checkbox">
								<input
									type="radio"
									name="token-type"
									value={ _AssetType.ERC1155 }
									checked={ inputAssetType === _AssetType.ERC1155 }
									onChange={() => {
										setInputAssetType(_AssetType.ERC1155);
										setInputCollateralAddress('');
										setInputCollateralAmount('');
										setInputCollateralTokenId('');
										setErc20Balance(undefined);
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">{ currentChain?.EIPPrefix || 'ERC' }-1155</span>
							</label>
						</div>
					</div>
					{ getCollateralItemAlert() }
					{ getCollateralNativeBlock() }
					{ getCollateralERC20Block() }
					{ getCollateralERC721Block() }
					{ getCollateralERC1155Block() }
				</div>

		)
	}

	return (
		<div className="modal modal-lg">
			<div className="modal__inner">
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">
						<div
							className="modal__close"
							onClick={() => { closePopup() }}
						>
							<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
								<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
							</svg>
						</div>


						<div className="c-add">
							<div className="c-add__text">
								<div className="h2">Add Collateral</div>
								{ maxSlots !== undefined ? (
									<div className="d-flex">
										<img className="mr-2" src={ icon_attention } alt="" />
										<span className="text-muted">мах { maxSlots } entries</span>
									</div>
								) : null }
							</div>
							{ getCollateralBlock() }
						</div>

						{ getCollateralRows() }

						{ getAddValueBtn() }

					</div>
				</div>
			</div>
		</div>
	)
}
