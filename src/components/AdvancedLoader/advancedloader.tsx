
import Tippy from "@tippyjs/react";
import React from "react";
import {
	AdvancedLoaderStageType,
	AdvancedLoaderType,
	AdvancedLoadingStatus
} from "../../_types";

import i_loader_orange from "../../static/pics/loader-orange.svg";

export default function AdvancedLoader(props: AdvancedLoaderType) {

	const getStageRow = (item: AdvancedLoaderStageType) => {
		if ( item.status === AdvancedLoadingStatus.queued ) {
			return (
				<div
					key={item.id}
					className="c-approve__step in-queue"
				>
					<div className="row">
						<div className="col-12 col-sm-auto order-2 order-sm-1">
							{
								item.current && item.total ?
								(
									<React.Fragment>
										<span className="current">{ item.current }</span>
										{ ' ' }
										/
										{ ' ' }
										{ item.total }
										{ ' ' }
									</React.Fragment>
								)
								: null
							}
							<span className="ml-2">
								{
									item.text.length < 40 ? item.text : (
										<Tippy
											content={ item.text }
											appendTo={ document.getElementsByClassName("wrapper")[0] }
											trigger='mouseenter'
											interactive={ false }
											arrow={ false }
											maxWidth={ 512 }
										>
											<span>{ item.text.slice(0, 40) }</span>
										</Tippy>
									)
								}
								<span className="dots">...</span>
							</span>
						</div>
						<div className="col-12 col-sm-auto order-1 order-sm-1"> </div>
					</div>
				</div>
			)
		}

		if ( item.status === AdvancedLoadingStatus.loading ) {
			return (
				<div
					key={item.id}
					className="c-approve__step active"
				>
					<div className="row">
						<div className="col-12 col-sm-auto order-2 order-sm-1">
							{
								item.current && item.total ?
								(
									<React.Fragment>
										<span className="current">{ item.current }</span>
										{ ' ' }
										/
										{ ' ' }
										{ item.total }
										{ ' ' }
									</React.Fragment>
								)
								: null
							}
							<span className="ml-2">
							{
									item.text.length < 40 ? item.text : (
										<Tippy
											content={ item.text }
											appendTo={ document.getElementsByClassName("wrapper")[0] }
											trigger='mouseenter'
											interactive={ false }
											arrow={ false }
											maxWidth={ 512 }
										>
											<span>{ item.text.slice(0, 40) }</span>
										</Tippy>
									)
								}
								<span className="dots">...</span>
							</span>
						</div>
						<div className="col-12 col-sm-auto order-1 order-sm-1">
							<div className="status">
								<img className="loader" src={ i_loader_orange } alt="" />
							</div>
						</div>
					</div>
				</div>
			)
		}
		if ( item.status === AdvancedLoadingStatus.complete ) {
			return (
				<div
					key={item.id}
					className="c-approve__step completed"
				>
					<div className="row">
						<div className="col-12 col-sm-auto order-2 order-sm-1">
							{
								item.current && item.total ?
								(
									<React.Fragment>
										<span className="current">{ item.current }</span>
										{ ' ' }
										/
										{ ' ' }
										{ item.total }
										{ ' ' }
									</React.Fragment>
								)
								: null
							}
							{
								item.text.length < 40 ? item.text : (
									<Tippy
										content={ item.text }
										appendTo={ document.getElementsByClassName("wrapper")[0] }
										trigger='mouseenter'
										interactive={ false }
										arrow={ false }
										maxWidth={ 512 }
									>
										<span>{ item.text.slice(0, 40) }</span>
									</Tippy>
								)
							}
						</div>
						<div className="col-12 col-sm-auto order-1 order-sm-1">
							<div className="status">
								<b className="text-green">Completed</b>
							</div>
						</div>
					</div>
				</div>
			)
		}
	}

	if ( !props.stages.length ) { return null; }

	return (
		<div className="modal">
			<div className="modal__inner">
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">

						<div className="modal__header">
							<div className="h2">{ props.title }</div>
						</div>

						<div className="c-approve">
							{
								props.stages
									.sort((item, prev) => { return item.sortOrder - prev.sortOrder })
									.map((item) => { return getStageRow(item) })
							}
						</div>

					</div>
				</div>
			</div>
		</div>
	)
}