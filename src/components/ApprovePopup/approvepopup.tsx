import {
	NFTorWNFT,
	Web3,
	_AssetType,
	setApprovalERC1155,
	setApprovalERC721,
	setApprovalForAllERC721
} from "envelop-client-core";
import { useState } from "react";
import {
	useAppDispatch,
	useAppSelector
} from "../../redux/store";
import {
	clearInfoModal,
	setInfoModal,
	setLoading,
	unsetLoading
} from "../../redux/basicActions";
import { ModalTypes } from "../../_types";
import Web3Dispatcher from "../Web3Dispatcher";

type ApprovePopupProps = {
	token   : NFTorWNFT,
	closePopup     : Function,
}

export default function ApprovePopup(props: ApprovePopupProps) {

	const { token, closePopup } = props;

	const [ approveToAddress, setApproveToAddress ] = useState('');

	const currentChain = useAppSelector((state) => { return state.currentChain });
	const userAccount  = useAppSelector((state) => { return state.account      });

	const dispatch = useAppDispatch();

	const approveSuccess = (data: any) => {
		dispatch(unsetLoading());

		dispatch(setInfoModal({
			type: ModalTypes.success,
			data: {
				text: `Our tokens has been approved for: ${approveToAddress}`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => { dispatch(clearInfoModal()) }
				}],
				links: [{
					text: `View on ${currentChain?.explorerName || 1}`,
					url: `${currentChain?.explorerBaseUrl || 1}/tx/${data.transactionHash}`
				}]
			}
		}));
		closePopup();
	}
	const approveError = (e: any) => {
		console.log(e);
		dispatch(unsetLoading());

		let errorMsg = '';
		try {
			if ( 'message' in e ) {
				errorMsg = e.message;
				try {
					const errorParsed = JSON.parse(e.message.slice(e.message.indexOf('\n')));
					errorMsg = errorParsed.originalError.message;
				} catch(ignored) {}
			} else {
				errorMsg = `${e}`;
			}
		} catch (ignored) {
			errorMsg = `${e}`;
		}

		let links = undefined;
		try {
			if ('transactionHash' in e) {
				links = [{ text: `${currentChain?.explorerName || ''}`, url: `${currentChain?.explorerBaseUrl || ''}/tx/${e.transactionHash}` }];
			} else {
				try {
					const errorParsed = JSON.parse(e.message.slice(e.message.indexOf('\n')));
					const txHash = errorParsed.transactionHash;
					if ( txHash ) {
						links = [{ text: `${currentChain?.explorerName || ''}`, url: `${currentChain?.explorerBaseUrl || ''}/tx/${txHash}` }];
					}
				} catch(ignored) {}
			}
		} catch (ignored) {}

		dispatch(setInfoModal({
			type: ModalTypes.error,
			data: {
				text: `Cannot approve token': ${errorMsg}`,
				buttons: undefined,
				links: links,
			}
		}));
	}

	const approveSubmit = async (web3: Web3) => {
		if ( !userAccount ) { return; }

		dispatch(setLoading('Waiting for approve'));

		if ( token.assetType === _AssetType.ERC1155 ) {
			return;
		} else {
			setApprovalERC721(
				web3,
				token.contractAddress,
				token.tokenId,
				userAccount.userAddress,
				approveToAddress
			)
				.then((data) => { approveSuccess(data) })
				.catch((e) => { approveError(e) });
		}
	}

	const approveAllSubmit = async (web3: Web3) => {

		if ( !userAccount ) { return; }

		dispatch(setLoading('Waiting for approve'));

		if ( token.assetType === _AssetType.ERC1155 ) {
			setApprovalERC1155(
				web3,
				token.contractAddress,
				userAccount.userAddress,
				approveToAddress
			)
				.then((data) => { approveSuccess(data) })
				.catch((e) => { approveError(e) });
		} else {
			setApprovalForAllERC721(
				web3,
				token.contractAddress,
				userAccount.userAddress,
				approveToAddress
			)
				.then((data) => { approveSuccess(data) })
				.catch((e) => { approveError(e) });
		}
	}

	const getApproveAllSubmitBtn = () => {
		if (
			approveToAddress === '' ||
			!Web3.utils.isAddress(approveToAddress)
		) {
			return (
				<div className="col">
					<button
						className="btn"
						type="button"
						disabled={ true }
					>{ 'Approve all' }</button>
				</div>
			)
		}
		return (
			<Web3Dispatcher.Consumer>
				{ web3dispatcher => (
					<div className="col">
						<button
							className="btn"
							type="button"
							onClick={async () => {
								let web3;
								try {
									web3 = await web3dispatcher.getForce();
								} catch(e: any) {
									console.log('You shouldda connect wallet');
									return;
								}

								approveAllSubmit(web3)
							}}
						>{ 'Approve all' }</button>
					</div>
				)}
			</Web3Dispatcher.Consumer>
		)
	}
	const getApproveSubmitBtn = () => {
		if ( token.assetType  === _AssetType.ERC1155 ) { return null; }
		if (
			approveToAddress === '' ||
			!Web3.utils.isAddress(approveToAddress)
		) {
			return (
				<div className="col">
					<button
						className="btn"
						type="button"
						disabled={ true }
					>{ 'Approve' }</button>
				</div>
			)
		}
		return (
			<Web3Dispatcher.Consumer>
				{ web3dispatcher => (
					<div className="col">
						<button
							className="btn"
							type="button"
							onClick={async () => {
								let web3;
								try {
									web3 = await web3dispatcher.getForce();
								} catch(e: any) {
									console.log('You shouldda connect wallet');
									return;
								}

								approveSubmit(web3)
							}}
						>{ 'Approve' }</button>
					</div>
				)}
			</Web3Dispatcher.Consumer>
		)
	}

	return (
		<div className="modal">
			<div
				className="modal__inner"
				onClick={(e) => {
					e.stopPropagation();
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						closePopup();
					}
				}}
			>
				<div className="modal__bg"></div>
				<div className="container">
					<div className="modal__content">
						<div
							className="modal__close"
							onClick={() => { closePopup() }}
						>
							<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
								<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
							</svg>
						</div>
						<div className="c-add">
							<div className="c-add__text">
								<div className="h2">{ 'Set approval for token' }</div>
							</div>
							<div className="c-add__coins">
								<div className="c-add__form">

										<div className="form-row">
											<div className="col">
												<input
													className="input-control"
													type="text"
													placeholder={ 'Paste address' }
													value={ approveToAddress }
													onChange={(e) => { setApproveToAddress(e.target.value) } }
												/>
											</div>
											{ getApproveSubmitBtn() }
											{ getApproveAllSubmitBtn() }
										</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)

}