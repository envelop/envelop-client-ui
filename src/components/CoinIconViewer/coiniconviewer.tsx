
import { MouseEventHandler } from 'react';
import {
	BigNumber,
	CollateralItem,
	ERC20Type,
	_AssetType,
	getERC20Params
} from 'envelop-client-core';
import {
	useAppDispatch,
	useAppSelector
} from '../../redux/store';
import {
	erc20TokenUpdate
} from '../../redux/basicActions';
import default_icon from 'envelop-client-core/static/pics/coins/_default.svg';
import default_nft from 'envelop-client-core/static/pics/_default_nft.svg';

type CoinIconViewerProps = {
	tokens        : Array<CollateralItem>,
	onClick?      : MouseEventHandler,
	onMouseEnter? : MouseEventHandler,
	onMouseLeave? : MouseEventHandler,
}

export default function CoinIconViewer(props: CoinIconViewerProps) {

	const currentChain = useAppSelector((state) => { return state.currentChain });
	const erc20List    = useAppSelector((state) => { return state.erc20List });
	const dispatch     = useAppDispatch();

	const getCollateralItem = (item: CollateralItem) => {

		// native token
		if ( item.assetType === _AssetType.native && item.amount ) {
			return ( <span className="i-coin" key={ 'native' }><img src={ currentChain?.tokenIcon || default_icon } alt="" /></span> )
		}

		if ( item.assetType === _AssetType.ERC20 && item.amount ) {
			// Common ERC20
			const foundERC20 = erc20List.find((iitem: ERC20Type) => {
				if ( !iitem.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase()
			});
			if ( foundERC20 ) {
				// known ERC20
				return ( <span className="i-coin" key={ foundERC20.contractAddress }><img src={ foundERC20.icon || default_icon } alt="" /></span> )
			} else {
				// unknown ERC20
				getERC20Params(currentChain?.chainId || 1, item.contractAddress)
					.then((data) => { if ( data ) { dispatch(erc20TokenUpdate(data)) } })
				return ( <span className="i-coin" key={ item.contractAddress }><img src={ default_icon } alt="" /></span> )
			}
		}

		if ( item.assetType === _AssetType.ERC721 ) {
			return ( <span className="i-coin" key={ `${item.contractAddress}${item.tokenId}` }><img src={ item.tokenImg || default_nft } alt="" /></span> )
		}

		if ( item.assetType === _AssetType.ERC1155 ) {
			return ( <span className="i-coin" key={ `${item.contractAddress}${item.tokenId}` }><img src={ item.tokenImg || default_nft } alt="" /></span> )
		}

		return null;
	}

	return (
		<div
			className="coins"
			onClick={ props.onClick }
			onMouseEnter={ props.onMouseEnter }
			onMouseLeave={ props.onMouseLeave }
		>
			{
				props.tokens
					.sort((item, prev) => {
						if ( item.assetType < prev.assetType ) { return -1 }
						if ( item.assetType > prev.assetType ) { return  1 }

						if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
						if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

						if ( item.tokenId && prev.tokenId ) {
							try {
								if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
									if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return -1 }
									if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return  1 }
								}
								const itemTokenIdNumber = new BigNumber(item.tokenId);
								const prevTokenIdNumber = new BigNumber(prev.tokenId);

								if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return -1 }
								if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return  1 }
							} catch ( ignored ) {
								if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return -1 }
								if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return  1 }
							}
						}

						return 0
					})
					.slice(0, 10)
					.map((item) => { return getCollateralItem(item); })
			}
		</div>
	)

}