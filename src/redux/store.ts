import {
	configureStore,
	ThunkAction,
	Action
} from '@reduxjs/toolkit';
import {
	TypedUseSelectorHook,
	useDispatch,
	useSelector
} from 'react-redux';
import { StateType } from '../_types';
import { basicReducer } from './reducer';

export const initialBasicState: StateType = {
	_advancedLoading : undefined,
	_info            : undefined,
	account          : undefined,
	availableChains: [],
	erc20List: [],
}

export const store = configureStore({
	reducer: basicReducer,
	devTools: true,
	preloadedState: initialBasicState,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export type StoreType = typeof store;
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
