
import { initialBasicState } from "./store"
import {
	AdvancedLoaderStageType,
	ModalTypes,
	StateType
} from "../_types"

export const basicReducer = (state = initialBasicState, action: any): StateType => {

	switch ( action.type ) {

		case 'RESET_APP_DATA': {
			return {
				...initialBasicState,
			};
		}

		case 'CREATE_ADVANCED_LOADING': {
			return {
				...state,
				_advancedLoading: action.payload,
			}
		}
		case 'CLEAR_ADVANCED_LOADING': {
			return {
				...state,
				_advancedLoading: undefined,
			}
		}
		case 'UPDATE_STEP_ADVANCED_LOADING': {
			if ( !state._advancedLoading ) { return state; }

			let updatedStages = state._advancedLoading.stages;
			action.payload.forEach((item: AdvancedLoaderStageType) => {
				const foundStage = updatedStages.find((iitem) => { return iitem.id.toLowerCase() === item.id.toLowerCase() });
				if ( !foundStage ) {
					updatedStages = [
						...updatedStages.filter((iitem) => {
							return iitem.id.toLowerCase() !== item.id.toLowerCase()
						}),
						item
					]
				}

				updatedStages = [
					...updatedStages.filter((iitem) => {
						return iitem.id.toLowerCase() !== item.id.toLowerCase()
					}),
					{
						...foundStage,
						...item
					}
				]
			})

			return {
				...state,
				_advancedLoading: {
					...state._advancedLoading,
					stages: updatedStages
				},
			}
		}

		case 'SET_INFO_MODAL': {
			return {
				...state,
				_info: action.payload,
			}
		}
		case 'CLEAR_INFO_MODAL': {
			return {
				...state,
				_info: undefined,
			}
		}
		case 'SET_LOADING': {
			return {
				...state,
				_info: {
					type: ModalTypes.loading,
					data: {
						text: action.payload,
					}
				}
			}
		}
		case 'UNSET_LOADING': {
			return {
				...state,
				_info: undefined,
				_advancedLoading: undefined,
			}
		}

		case 'UPDATE_ACCOUNT': {
			return {
				...state,
				account: {
					...state.account,
					...action.payload
				}
			}
		}

		case 'ERC20_TOKEN_UPDATE': {
			return {
				...state,
				erc20List: [
					...state.erc20List.filter((item) => { return item.contractAddress.toLowerCase() !== action.payload.contractAddress.toLowerCase() }),
					action.payload,
				],
			}
		}
		case 'ERC20_TOKEN_UPDATE_BALANCE': {
			const foundToken = state.erc20List.find((item) => { return item.contractAddress.toLowerCase() === action.payload.contractAddress.toLowerCase() });
			if ( !foundToken ) { return state; }

			const updatedToken = {
				...foundToken,
				balance: action.payload.balance,
				allowance: [
					...foundToken.allowance.filter((item) => { return item.allowanceTo.toLowerCase() !== action.payload.allowance.allowanceTo.toLowerCase() }),
					action.payload.allowance
				]
			}

			return {
				...state,
				erc20List: [
					...state.erc20List.filter((item) => { return item.contractAddress.toLowerCase() !== action.payload.contractAddress.toLowerCase() }),
					updatedToken
				],
			}
		}

		case 'SET_CURRENT_CHAIN': {
			return {
				...state,
				currentChain: action.payload,
			}
		}

		default: { return state }

	}
}