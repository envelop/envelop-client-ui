import {
	BigNumber,
	ChainType,
	ERC20Balance,
	ERC20Type
} from "envelop-client-core"
import {
    AdvancedLoaderStageType,
    AdvancedLoaderType,
	InfoModalProps
} from "../_types"

export const createAdvancedLoading = (payload: AdvancedLoaderType) => {
	return {
		type: 'CREATE_ADVANCED_LOADING',
		payload,
	}
}
export const clearAdvancedLoading = () => {
	return {
		type: 'CLEAR_ADVANCED_LOADING',
	}
}
export const updateStepAdvancedLoading = (payload: Array<Partial<AdvancedLoaderStageType>>) => {
	return {
		type: 'UPDATE_STEP_ADVANCED_LOADING',
		payload,
	}
}
export const setInfoModal = (payload: InfoModalProps) => {
	return {
		type: 'SET_INFO_MODAL',
		payload,
	}
}
export const clearInfoModal = () => {
	return {
		type: 'CLEAR_INFO_MODAL',
	}
}
export const setLoading = (payload: string) => {
	return {
		type: 'SET_LOADING',
		payload,
	}
}
export const unsetLoading = () => {
	return {
		type: 'UNSET_LOADING',
	}
}
export const resetAppData = () => {
	return {
		type: 'RESET_APP_DATA',
	}
}

export const requestChain = ( payload: number | undefined ) => {
	return {
		type: 'REQUEST_CHAIN',
		payload
	}
}

export const erc20TokenUpdate = ( payload: ERC20Type ) => {
	return {
		type: 'ERC20_TOKEN_UPDATE',
		payload
	}
}
export const erc20TokenUpdateBalance = ( payload: ERC20Balance ) => {
	return {
		type: 'ERC20_TOKEN_UPDATE_BALANCE',
		payload
	}
}

export const updateAccount = (payload: {
	userAddress?: string,
	balanceNative?: BigNumber,
}) => {
	return {
		type: 'UPDATE_ACCOUNT',
		payload
	}
}

export const setCurrentChain = (payload: ChainType) => {
	return {
		type: 'SET_CURRENT_CHAIN',
		payload
	}
}